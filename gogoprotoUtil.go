package ygen

import (
	"errors"

	"github.com/gogo/protobuf/protoc-gen-gogo/descriptor"
)

// 得到 proto文件中 Message的前导注释
func GetProtoMsgLeadingCommentsgogo(fd *descriptor.FileDescriptorProto, msg *descriptor.DescriptorProto) string {

	locs := fd.SourceCodeInfo.Location
	msgs := fd.MessageType

	if locs == nil {
		return ""
	}

	msgIdx := -1

	//find msg idx
	for idx, msgItem := range msgs {
		if msgItem == msg {
			msgIdx = idx
			break
		}
	}

	if msgIdx < 0 {
		//not found the msg
		return ""
	}

	for _, loc := range locs {
		path := loc.Path

		if len(path) == 2 &&
			path[0] == 4 &&
			path[1] == int32(msgIdx) {
			if loc.LeadingComments != nil {
				return *loc.LeadingComments
			} else {
				break
			}
		}
	}

	return ""
}

// 得到 progo message中字段的前导注释
func GetProtoMsgFieldLeadingCommentsgogo(fd *descriptor.FileDescriptorProto, msg *descriptor.DescriptorProto, field *descriptor.FieldDescriptorProto) (error, string) {
	locs := fd.SourceCodeInfo.Location
	msgs := fd.MessageType
	fields := msg.Field

	if locs == nil {
		return nil, ""
	}

	msgIdx := -1

	//find msg idx
	for idx, msgItem := range msgs {
		if msgItem == msg {
			msgIdx = idx
			break
		}
	}

	if msgIdx < 0 {
		return errors.New("not found msg:" + *msg.Name), ""
	}

	fieldIdx := -1
	for idx, fieldItem := range fields {
		if fieldItem == field {
			fieldIdx = idx
			break
		}
	}

	if fieldIdx < 0 {
		return errors.New("not found field:" + *field.Name), ""
	}

	for _, loc := range locs {
		path := loc.Path

		if len(path) == 4 &&
			path[0] == 4 &&
			path[1] == int32(msgIdx) &&
			path[2] == 2 &&
			path[3] == int32(fieldIdx) {
			if loc.LeadingComments != nil {
				return nil, *loc.LeadingComments
			} else {
				break
			}
		}
	}

	return nil, ""
}

// Is this field repeated?
func IsFieldRepeatedgogo(field *descriptor.FieldDescriptorProto) bool {
	return field.Label != nil && *field.Label == descriptor.FieldDescriptorProto_LABEL_REPEATED
}
