package main

import (
	"io/ioutil"
	"os"
	"strings"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	"github.com/golang/protobuf/protoc-gen-go/generator"
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/ygen"
)

func main() {
	// Begin by allocating a generator. The request and response structures are stored there
	// so we can do error handling easily - the response structure contains the field to
	// report failure.
	g := generator.New()

	data, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		g.Error(err, "reading input")
	}

	if err := proto.Unmarshal(data, g.Request); err != nil {
		g.Error(err, "parsing input proto")
	}

	if len(g.Request.FileToGenerate) == 0 {
		g.Fail("no files to generate")
	}

	g.CommandLineParameters(g.Request.GetParameter())

	// Create a wrapped version of the Descriptors and EnumDescriptors that
	// point to the file that defines them.
	g.WrapTypes()

	g.SetPackageNames()
	g.BuildTypeNameMap()

	g.GenerateAllFiles()

	//patch struct tag for ykit
	g.Response = ykitPatch(g.Request, g.Response)

	// Send back the results.
	data, err = proto.Marshal(g.Response)
	if err != nil {
		g.Error(err, "failed to marshal output proto")
	}
	_, err = os.Stdout.Write(data)
	if err != nil {
		g.Error(err, "failed to write output proto")
	}
}

//patch resp from gogofaster to add ykit tag in generated go struct
func ykitPatch(req *plugin.CodeGeneratorRequest, resp *plugin.CodeGeneratorResponse) *plugin.CodeGeneratorResponse {
	reqFiles := req.GetProtoFile()
	for _, reqf := range reqFiles {
		respfilename := goutil.ExtractFilename(reqf.GetName()) + ".pb.go"
		for _, respf := range resp.GetFile() {
			if strings.Contains(*respf.Name, respfilename) {
				ykitPatchFile(reqf, respf)
			}
		}
	}
	return resp
}

func ykitPatchFile(reqf *descriptor.FileDescriptorProto, respf *plugin.CodeGeneratorResponse_File) {
	result := strings.Builder{}
	resultPos := 0
	origResult := *respf.Content

	if len(origResult) == 0 {
		return
	}

	for _, msg := range reqf.MessageType {
		if !strings.HasPrefix(*msg.Name, "Db") {
			//如果不是以Db开头则不处理，规定数据库相关的表message必须以Db开头
			continue
		}
		for _, field := range msg.Field {
			ykitTag := genYkitTag(reqf, msg, field)

			if len(ykitTag) > 0 {
				msgStruct := "type " + *msg.Name + " struct"
				msgStructPos := strings.Index(origResult, msgStruct)
				if msgStructPos < 0 {
					//log.Fatal("can not find msg struct:", msgStruct, *respf.Name)
					continue
				}
				fieldJsonTag := *field.Name + ",omitempty"

				tagPos := goutil.StrIndex(origResult, fieldJsonTag, msgStructPos)
				if tagPos < 0 {
					//log.Fatal("can not get json tag:", *msg.Name, fieldJsonTag)
					continue
				}
				newPos := tagPos + len(fieldJsonTag) + 1
				result.WriteString(origResult[resultPos:newPos])
				result.WriteString(ykitTag)
				resultPos = newPos
			}
		}
	}

	result.WriteString(origResult[resultPos:])

	r := result.String()
	respf.Content = &r
}

func genYkitTag(reqf *descriptor.FileDescriptorProto, msg *descriptor.DescriptorProto, field *descriptor.FieldDescriptorProto) string {
	_, filedComment := ygen.GetProtoMsgFieldLeadingComments(reqf, msg, field)

	return ygen.GenYkitTag(filedComment)
}
