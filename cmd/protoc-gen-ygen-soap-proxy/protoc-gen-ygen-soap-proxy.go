package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"text/template"

	"github.com/golang/glog"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	plugin_go "github.com/golang/protobuf/protoc-gen-go/plugin"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/ygen"
	"google.golang.org/protobuf/types/pluginpb"
)

var request plugin_go.CodeGeneratorRequest

//pkg.msg -> msg
//var allMsgTypes = make(map[string]*descriptor.DescriptorProto)

func main() {
	data, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		glog.Fatalf("error: reading input: %v", err)
	}

	var response plugin_go.CodeGeneratorResponse
	if err := proto.Unmarshal(data, &request); err != nil {
		glog.Fatalf("error: parsing input proto: %v", err)
	}

	ProtoPkg2GoPkgMap = genProtoPkg2GoPkgMap(&request)

	genHttpHandlerFiles := genSoapHttpHandler(&request)

	for _, f := range genHttpHandlerFiles {
		response.File = append(response.File, f)
	}

	genXmlMsgFiles := genXmlGoMsg(&request)

	for _, f := range genXmlMsgFiles {
		response.File = append(response.File, f)
	}

	genSoapGrpcFiles := genSoapGrpcService(&request)

	for _, f := range genSoapGrpcFiles {
		response.File = append(response.File, f)
	}
	var SupportedFeatures uint64 = uint64(pluginpb.CodeGeneratorResponse_FEATURE_PROTO3_OPTIONAL)
	response.SupportedFeatures = &SupportedFeatures

	if data, err = proto.Marshal(&response); err != nil {
		glog.Fatalf("error: failed to marshal output proto: %v", err)
	}
	if _, err := os.Stdout.Write(data); err != nil {
		glog.Fatalf("error: failed to write output proto: %v", err)
	}

}

var msgCxx = regexp.MustCompile(`(.+)C\d+`)

func msgPdtName(msgname string) string {
	submatch := msgCxx.FindStringSubmatch(msgname)
	if submatch == nil {
		return msgname
	}

	return submatch[1]
}

func genXmlGoMsg(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenSoapXmlMsg(request)
	genFiles = append(genFiles, rpcGoItems...)

	return
}

type MsgXmlItem struct {
	ProtoFd *descriptor.FileDescriptorProto
	Msg     *descriptor.DescriptorProto
}

func (this MsgXmlItem) MsgGoName() string {
	return ygen.CamelCase(this.Msg.GetName())
}
func (this MsgXmlItem) MsgName() string {
	return this.Msg.GetName()
}

func (this MsgXmlItem) MsgXmlName() string {
	return msgPdtName(this.Msg.GetName())
}
func (this MsgXmlItem) FieldGoName(fno int) string {
	return ygen.CamelCase(this.Msg.Field[fno].GetName())
}
func (this MsgXmlItem) FieldName(fno int) string {
	return this.Msg.Field[fno].GetName()
}
func (this MsgXmlItem) FieldXmlTypeName(fno int) string {
	ftn := this.Msg.Field[fno].GetTypeName()
	if strings.HasPrefix(ftn, ".") {
		ftns := strings.Split(ftn, ".")
		return ftns[len(ftns)-1]
	}
	//ft := this.Msg.Field[fno].GetType()
	gotype, _ := ygen.FieldGoType(this.ProtoFd, this.Msg, this.Msg.Field[fno])

	return gotype

}
func (this MsgXmlItem) FieldElementXmlTypeName(fno int) string {
	ftn := this.Msg.Field[fno].GetTypeName()
	if strings.HasPrefix(ftn, ".") {
		ftns := strings.Split(ftn, ".")
		return ftns[len(ftns)-1]
	}
	//ft := this.Msg.Field[fno].GetType()
	gotype, _ := ygen.FieldElementGoType(this.ProtoFd, this.Msg, this.Msg.Field[fno])

	switch gotype {
	case "int", "int8", "int16", "int32", "uint", "uint8", "uint16", "uint32":
		return "int"
	case "int64", "uint64":
		return "long"

	case "float32", "float64":
		return "double"

	}

	return gotype

}
func (this MsgXmlItem) ToXml() string {
	r := ""

	for i := range this.Msg.Field {
		r += this.FieldToXml(i)
	}

	return r

}
func (this MsgXmlItem) FieldToXml(fno int) string {
	r := ""

	field := this.Msg.Field[fno]

	if field.GetName() == "faultInfo" {
		//don't convert faultInfo
		return r
	}

	isRepeated := ygen.IsFieldRepeated(field)
	isMsg := field.GetType() == descriptor.FieldDescriptorProto_TYPE_MESSAGE
	isOptional := ygen.IsFieldOptional(field)

	if isOptional {
		if isRepeated {
			//这个不应该存在
			glog.Fatal("err optional and repeated both exists", field)
		} else {
			if isMsg {
				r += this.FieldToXmlSimpleMsgOptional(fno)
			} else {
				r += this.FieldToXmlSimpleOptional(fno)
			}
		}

	} else {

		if isRepeated {
			if isMsg {
				r += this.FieldToXmlSimpleMsgRep(fno)
			} else {
				r += this.FieldToXmlSimpleRep(fno)
			}

		} else {
			if isMsg {
				r += this.FieldToXmlSimpleMsg(fno)
			} else {
				r += this.FieldToXmlSimple(fno)
			}
		}
	}

	return r

}

//go简单类型
func (this MsgXmlItem) FieldToXmlSimple(fno int) string {
	fieldname := this.FieldName(fno)
	fieldgoname := this.FieldGoName(fno)
	r := ` tplel=el.CreateElement("%fieldname%")
tplel.SetText(fmt.Sprint(this.%fieldgoname%))
`
	r = strings.ReplaceAll(r, "%fieldname%", fieldname)
	r = strings.ReplaceAll(r, "%fieldgoname%", fieldgoname)

	return r
}

//go opt简单类型
func (this MsgXmlItem) FieldToXmlSimpleOptional(fno int) string {
	fieldname := this.FieldName(fno)
	fieldgoname := this.FieldGoName(fno)
	r := ` if this.%fieldgoname%!=nil{
tplel=el.CreateElement("%fieldname%")
tplel.SetText(fmt.Sprint(*this.%fieldgoname%))
}
`
	r = strings.ReplaceAll(r, "%fieldname%", fieldname)
	r = strings.ReplaceAll(r, "%fieldgoname%", fieldgoname)
	return r
}

//go简单msg类型
func (this MsgXmlItem) FieldToXmlSimpleMsg(fno int) string {
	return this.FieldToXmlSimpleMsgOptional(fno)
}

//go opt简单msg类型
func (this MsgXmlItem) FieldToXmlSimpleMsgOptional(fno int) string {
	fieldname := this.FieldName(fno)
	fieldgoname := this.FieldGoName(fno)
	r := ` if this.%fieldgoname%!=nil{
tplel=this.%fieldgoname%.ToXml("%fieldname%")
if tplel!=nil{
	el.AddChild(tplel)
}
}
`
	r = strings.ReplaceAll(r, "%fieldname%", fieldname)
	r = strings.ReplaceAll(r, "%fieldgoname%", fieldgoname)

	return r
}

//go简单类型
func (this MsgXmlItem) FieldToXmlSimpleRep(fno int) string {
	fieldname := this.FieldName(fno)
	fieldgoname := this.FieldGoName(fno)
	fieldxmltypename := this.FieldElementXmlTypeName(fno)
	r := ` tplel=el.CreateElement("%fieldname%")
	for _,item:=range this.%fieldgoname%{
	repel=tplel.CreateElement("%fieldxmltypename%")
	repel.SetText(fmt.Sprint(item))
}
`
	r = strings.ReplaceAll(r, "%fieldname%", fieldname)
	r = strings.ReplaceAll(r, "%fieldgoname%", fieldgoname)
	r = strings.ReplaceAll(r, "%fieldxmltypename%", fieldxmltypename)

	return r
}

//go简单msg类型
func (this MsgXmlItem) FieldToXmlSimpleMsgRep(fno int) string {
	fieldname := this.FieldName(fno)
	fieldgoname := this.FieldGoName(fno)
	fieldxmltypename := this.FieldXmlTypeName(fno)
	r := ` tplel=el.CreateElement("%fieldname%")
	for _,item:=range this.%fieldgoname%{
	repel=item.ToXml("")
	if repel!=nil{
	tplel.AddChild(repel)
}
}
`
	r = strings.ReplaceAll(r, "%fieldname%", fieldname)
	r = strings.ReplaceAll(r, "%fieldgoname%", fieldgoname)
	r = strings.ReplaceAll(r, "%fieldxmltypename%", fieldxmltypename)

	return r
}

func (this MsgXmlItem) FromXml() string {
	var r = ""
	for i, field := range this.Msg.Field {

		isRepeated := ygen.IsFieldRepeated(field)
		isMsg := field.GetType() == descriptor.FieldDescriptorProto_TYPE_MESSAGE
		isOptional := ygen.IsFieldOptional(field)

		if isOptional {
			if isRepeated {
				//这个不应该存在
				glog.Fatal("err optional and repeated both exists", field)
			} else {
				if isMsg {
					r += this.FieldFromXmlSimpleMsgOptional(i)
				} else {
					r += this.FieldFromXmlSimpleOptional(i)
				}
			}
		} else {
			if isRepeated {
				if isMsg {
					r += this.FieldFromXmlSimpleMsgRep(i)
				} else {
					r += this.FieldFromXmlSimpleRep(i)
				}
			} else {
				if isMsg {
					r += this.FieldFromXmlSimpleMsg(i)
				} else {
					r += this.FieldFromXmlSimple(i)
				}
			}
		}
	}

	return r
}

//go简单类型
func (this MsgXmlItem) FieldFromXmlSimple(fno int) string {
	field := this.Msg.Field[fno]
	r := `
	// %fieldgoname% type: %fieldtype%
	elem = el.FindElement("//%fieldname%")
	if elem!=nil{
	this.%fieldgoname% = nms.Str2%fieldtype%(elem.Text())
	}
`

	fieldelementtype := strings.ReplaceAll(field.Type.String(), "[", "_")
	fieldelementtype = strings.ReplaceAll(fieldelementtype, "]", "_")
	fieldelementtype = strings.ReplaceAll(fieldelementtype, "*", "_")
	r = strings.ReplaceAll(r, "%fieldtype%", fieldelementtype)
	r = strings.ReplaceAll(r, "%fieldname%", field.GetName())
	r = strings.ReplaceAll(r, "%fieldgoname%", ygen.CamelCase(field.GetName()))

	return r
}

//go opt简单类型
func (this MsgXmlItem) FieldFromXmlSimpleOptional(fno int) string {
	field := this.Msg.Field[fno]
	r := `
	// %fieldgoname% type: %fieldtype%
	elem = el.FindElement("//%fieldname%")
	if elem!=nil{
	val:= nms.Str2%fieldtype%(elem.Text())
	this.%fieldgoname% = &val
	}
`
	fieldelementtype := strings.ReplaceAll(field.Type.String(), "[", "_")
	fieldelementtype = strings.ReplaceAll(fieldelementtype, "]", "_")
	fieldelementtype = strings.ReplaceAll(fieldelementtype, "*", "_")
	r = strings.ReplaceAll(r, "%fieldtype%", fieldelementtype)
	r = strings.ReplaceAll(r, "%fieldname%", field.GetName())
	r = strings.ReplaceAll(r, "%fieldgoname%", ygen.CamelCase(field.GetName()))

	return r
}

//go简单msg类型
func (this MsgXmlItem) FieldFromXmlSimpleMsg(fno int) string {
	field := this.Msg.Field[fno]
	fieldgotype, _ := ygen.FieldGoType(this.ProtoFd, this.Msg, field)
	r := `st_%fieldname% := &%fieldgotype%{}
elem = el.FindElement("//%fieldname%")
	if elem!=nil{
	st_%fieldname%.FromXml(elem)
	this.%fieldgoname%=st_%fieldname%
	}
`

	r = strings.ReplaceAll(r, "%fieldtype%", field.Type.String())
	r = strings.ReplaceAll(r, "%fieldgotype%", fieldgotype)
	r = strings.ReplaceAll(r, "%fieldname%", msgPdtName(field.GetName()))
	r = strings.ReplaceAll(r, "%fieldgoname%", ygen.CamelCase(field.GetName()))

	return r
}

//go opt简单msg类型
func (this MsgXmlItem) FieldFromXmlSimpleMsgOptional(fno int) string {
	return this.FieldFromXmlSimpleMsg(fno)
}

//go rep 类型
func (this MsgXmlItem) FieldFromXmlSimpleRep(fno int) string {
	field := this.Msg.Field[fno]
	var name = field.GetName()
	fieldgotype, _ := ygen.FieldGoType(this.ProtoFd, this.Msg, field)
	r := `par := el.FindElement("//%name%")
		if par!=nil{
			els := par.ChildElements()
			if len(els)!=0{
				var datas = make(%fieldgotypes% ,0)
				for i := range els{
					datas = append(datas, nms.Str2%fieldgotype%(els[i].Text()))
				}
				this.%fieldgoname%=datas
			}
		}
`

	fieldelementtype := strings.ReplaceAll(field.Type.String(), "[", "_")
	fieldelementtype = strings.ReplaceAll(fieldelementtype, "]", "_")
	fieldelementtype = strings.ReplaceAll(fieldelementtype, "*", "_")

	r = strings.ReplaceAll(r, "%fieldgotype%", fieldelementtype)
	r = strings.ReplaceAll(r, "%fieldgotypes%", fieldgotype)
	r = strings.ReplaceAll(r, "%fieldgoname%", ygen.CamelCase(name))
	return r
}

//go rep msg类型
func (this MsgXmlItem) FieldFromXmlSimpleMsgRep(fno int) string {
	field := this.Msg.Field[fno]
	var name = field.GetName()
	fieldname := strings.TrimLeft(field.GetName(), ".")
	//ygen.CamelCase(
	//fieldgotype := FieldGoType(field.GetTypeName())
	fieldgotype, _ := ygen.FieldGoType(this.ProtoFd, this.Msg, field)
	//传数组
	r := `par := el.FindElement("//%fieldname%")
		if par!=nil{
			els:=par.ChildElements()
			if len(els)!=0{
			var datas = make(%fieldgotypes%,0)
			for i := range els {
				st := &%fieldgotype%{}
				st.FromXml(els[i])
				datas = append(datas, st)
			}
			this.%fieldgoname%=datas
		}
}
`
	r = strings.ReplaceAll(r, "%fieldname%", fieldname)
	r = strings.ReplaceAll(r, "%fieldgotypes%", fieldgotype)
	r = strings.ReplaceAll(r, "%fieldgotype%", strings.TrimLeft(fieldgotype, "[]*"))
	r = strings.ReplaceAll(r, "%fieldgoname%", ygen.CamelCase(strings.Trim(name, "\n")))
	return r
	//return "FieldFromXmlSimpleMsgRep\n"
}

// 生成proto go struct from/to xml
func YGenSoapXmlMsg(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	var fd *descriptor.FileDescriptorProto

	rpcCRUDHead := `//Package {{.package}}  generated by ygen-gocrud. DO NOT EDIT.
//source: {{.original_file}}
package {{.package}}

import (
    "encoding/xml"
	"github.com/beevik/etree"
	"fmt"
	"pdtnmssoap/nms"
`
	importEnd := ")\n"

	xmlTpl := `func (this*{{.MsgGoName}})ToXml(tagname string)(el*etree.Element){
	var tplel,repel*etree.Element
	_ = tplel
    _ = repel 
	if len(tagname)==0{
	tagname="{{.MsgXmlName}}"
	}
	el=etree.NewElement(tagname)
	{{.ToXml}}
	return el
}

func (this*{{.MsgGoName}})FromXml(el*etree.Element){
	var elem *etree.Element
	if el == nil {
		return
	}
	{{.FromXml}}
}
`

	//proto pkgname -> go pkgname
	protopkgname2gopkgname := make(map[string]string)

	for _, fd = range request.GetProtoFile() {
		protopkgname2gopkgname[fd.GetPackage()] = fd.GetOptions().GetGoPackage()

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			continue
		}
		if len(fd.MessageType) == 0 {
			continue
		}

		tHead := template.Must(template.New("soap-xml.go_head").Parse(rpcCRUDHead))
		sqlHeadData := map[string]interface{}{
			"original_file": fd.GetName(),
			"package":       fd.GetPackage(),
			"goPkg":         fd.Options.GetGoPackage(),
		}

		var tplBytes bytes.Buffer
		if err := tHead.Execute(&tplBytes, sqlHeadData); err != nil {
			glog.Fatal(err)
		}

		rpcFileHeadContent := tplBytes.String()

		additionalImportMap := make(map[string]string)

		rpcFileContent := ""

		rpcFilename := goutil.ExtractFilename(fd.GetName()) + ".xml.go"

		space4 := "    "

		tXmlItem := template.Must(template.New("soap-xml").Parse(xmlTpl))

		for _, msg := range fd.MessageType {
			//for import
			for _, field := range msg.Field {
				if strings.HasPrefix(field.GetTypeName(), ".") {
					importpkg := strings.Split(field.GetTypeName(), ".")[1]
					additionalImportMap[importpkg] = importpkg
				}

			}

			msgXmlItem := MsgXmlItem{
				ProtoFd: fd,
				Msg:     msg,
			}

			var tplBytes bytes.Buffer
			if err := tXmlItem.Execute(&tplBytes, msgXmlItem); err != nil {
				glog.Fatal(err)
			}

			rpcFileContent += tplBytes.String()

		}

		//fix import
		addImport := ""
		currentGoPkg := fd.GetOptions().GetGoPackage()
		for protopkg := range additionalImportMap {
			gopkg := protopkgname2gopkgname[protopkg]
			if gopkg != currentGoPkg {
				addImport += space4 + "\"" + gopkg + "\"\n"
			}
		}

		genFile := &plugin_go.CodeGeneratorResponse_File{
			Name:    proto.String(fd.Options.GetGoPackage() + "/" + rpcFilename),
			Content: proto.String(rpcFileHeadContent + addImport + importEnd + rpcFileContent),
		}

		genFiles = append(genFiles, genFile)

	}

	return
}

func genMethodInputGoMsg(inputtype, pkgName string) string {
	if !strings.HasPrefix(inputtype, ".") {
		return ygen.CamelCase(inputtype)
	}

	slist := strings.Split(inputtype, ".")
	slist[len(slist)-1] = ygen.CamelCase(slist[len(slist)-1])
	if pkgName == slist[1] {
		return slist[len(slist)-1]
	}
	return strings.Join(slist[1:], ".")
}
func genMethodInputXmlMsg(inputtype string) string {
	if !strings.HasPrefix(inputtype, ".") {
		return inputtype
	}

	slist := strings.Split(inputtype, ".")
	return slist[len(slist)-1]
}

func genSoapGrpcService(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenSoapGrpcService(request)
	genFiles = append(genFiles, rpcGoItems...)

	return
}

type TsoapGrpcItem struct {
	Protofd *descriptor.FileDescriptorProto
	Service *descriptor.ServiceDescriptorProto
	Up      bool //is up
	Down    bool //if down
}

func (this TsoapGrpcItem) GenImports() []string {
	r := make([]string, 0)
	for _, meth := range this.Service.Method {
		cache := getPackageImportNames(meth)
		r = append(r, cache...)
	}

	return r
}

func (this TsoapGrpcItem) GenServiceStruct() string {
	structTpl := `// SoapGrpc{{.Service}}Server
type SoapGrpc{{.Service}}Server struct {
Unimplemented{{.Service}}Server
}
`
	structIml := template.Must(template.New("structTpl").Parse(structTpl))

	structData := map[string]interface{}{}
	structData["Service"] = this.Service.GetName()
	var structBytes bytes.Buffer
	if err := structIml.Execute(&structBytes, structData); err != nil {
		glog.Fatal(err)
	}

	return structBytes.String()
}

var ProtoPkg2GoPkgMap map[string]string

func genProtoPkg2GoPkgMap(request *plugin_go.CodeGeneratorRequest) map[string]string {
	r := make(map[string]string)

	for _, fd := range request.GetProtoFile() {
		r[fd.GetPackage()] = fd.Options.GetGoPackage()
	}

	return r

}

func getPackageImportNames(meth *descriptor.MethodDescriptorProto) []string {
	cache := make([]string, 0)
	cache = append(cache, ygen.GetPackageFromMsgName(meth.GetInputType()))
	cache = append(cache, ygen.GetPackageFromMsgName(meth.GetOutputType()))

	return cache
}

func (this TsoapGrpcItem) GenRpcMethod() string {
	methodTpl := `func (*SoapGrpc{{.Service}}Server) {{.MethodGoName}}(ctx context.Context, req *{{.InMsgGoName}}) (response *{{.OutMsgGoName}}, err error) {
	msgxml:=req.ToXml("")
	//make xml call msg add xml body
	soapreqxml:=nms.MakeSoapXmlReq(msgxml)

	target:=nmscommon.GetCtxSoapTarget(ctx)

	if nms.ProxyConfig.Debug{
		fmt.Println(time.Now(),"got rpc call /{{.Service}}/{{.MethodName}}:",req,"target:",target)
	}

	//make target
	{{if .Up}}
	if target==nil{
		target=nmscommon.GetDefaultSoapUpServerTarget()
	}
	{{end}}
	{{if .Down}}
	if target ==nil{
		return nil, errors.New("not provide soap target")
	}
	{{end}}
	//soap call
	var soapresponse *etree.Document
	soapresponse,err=nmscommon.MakeSoapCall(target,"/{{.Service}}/{{.MethodName}}",soapreqxml)
	if err!=nil{
		return nil,err
	}

	response=&{{.OutMsgGoName}}{}

	if nms.ProxyConfig.Debug{
		defer fmt.Println(time.Now(),"rpc call result /{{.Service}}/{{.MethodName}}:",response,"target:",target)
	}

	//response from xml
	responseel:=soapresponse.FindElement("//{{.OutMsgName}}")
	
	if responseel == nil {
		//find if fault 
		fault:=soapresponse.FindElement("//Fault")
		if fault != nil {
			response.FaultInfo=fault.Text()
			return response,nil
		}
		resDoc, err := soapresponse.WriteToString()
		if err != nil {
			resDoc = "\n doc writeToString err" + err.Error()
		}
		return nil, errors.New("can not found response xml element:" + resDoc)
	}
	response.FromXml(responseel)

	if nms.ProxyConfig.Debug{
		fmt.Println(time.Now(),"rpc call result /{{.Service}}/{{.MethodName}}:",response,"target:",target)
	}

	return response, nil
}

`

	r := ""

	methodTplParser := template.Must(template.New("rpcmethod").Parse(methodTpl))
	//paramTpl := make(map[string]interface{})
	for _, method := range this.Service.Method {
		methodData := map[string]interface{}{}

		//method head
		methodData["Service"] = this.Service.GetName()
		methodData["MethodGoName"] = ygen.CamelCase(method.GetName())
		methodData["MethodName"] = method.GetName()
		methodData["InMsgGoName"] = genMethodInputGoMsg(method.GetInputType(), this.Protofd.GetPackage())
		methodData["OutMsgGoName"] = genMethodInputGoMsg(method.GetOutputType(), this.Protofd.GetPackage())
		methodData["OutMsgName"] = genMethodInputXmlMsg(method.GetOutputType())

		//method body
		methodData["Up"] = this.Up
		methodData["Down"] = this.Down

		var methodContentBytes bytes.Buffer
		if err := methodTplParser.Execute(&methodContentBytes, methodData); err != nil {
			glog.Fatal(err)
		}
		r += methodContentBytes.String()
	}

	return r
}

// 生成grpc service
func YGenSoapGrpcService(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {

	var fd *descriptor.FileDescriptorProto

	rpcCRUDHead := `//Package {{.package}}  generated by ygen-gocrud. DO NOT EDIT.
//source: {{.original_file}}
package {{.package}}

import (
	"context"
	"github.com/beevik/etree"
	"pdtnmssoap/nms"
	"errors"
	"fmt"
	"time"
`
	importEnd := `
)
`

	grpcTpl := `
{{.GenServiceStruct}}
{{.GenRpcMethod}}

`

	for _, fd = range request.GetProtoFile() {

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			//glog.Println("no gen file:", fd.GetName())
			continue
		}

		if len(fd.Service) == 0 {
			continue
		}

		// 头部信息，package,imports
		rpcCRUDHeadIml := template.Must(template.New("rpcCRUDHead").Parse(rpcCRUDHead))
		rpcCRUDHeadData := map[string]interface{}{
			"original_file": fd.GetName(),
			"package":       fd.GetPackage(),
			"goPkg":         fd.Options.GetGoPackage(),
		}

		var rpcCRUDHeadBytes bytes.Buffer
		if err := rpcCRUDHeadIml.Execute(&rpcCRUDHeadBytes, rpcCRUDHeadData); err != nil {
			glog.Fatal(err)
		}
		rpcFileHeadContent := rpcCRUDHeadBytes.String()

		rpcFileContent := ""

		rpcFilename := goutil.ExtractFilename(fd.GetName()) + ".soapgrpc.go"

		protoImport := make(map[string]string)

		grpcTplParser := template.Must(template.New("grpc").Parse(grpcTpl))

		for _, srv := range fd.Service {
			srvItem := TsoapGrpcItem{
				Protofd: fd,
				Service: srv,
				Up:      true,
				Down:    false,
			}

			//需要判断service的注释，up？down
			comments, _ := ygen.GetProtoServiceLeadingComments(fd, srv)
			if strings.Contains(comments, "@down") {
				srvItem.Up = false
				srvItem.Down = true
			} else {
				//no need
			}

			srvImports := srvItem.GenImports()
			for _, srvImport := range srvImports {
				protoImport[srvImport] = srvImport
			}

			var tplBytes bytes.Buffer
			if err := grpcTplParser.Execute(&tplBytes, srvItem); err != nil {
				glog.Fatal(err)
			}

			rpcFileContent += tplBytes.String()

		}
		addImport := ""
		for _, impprotopkg := range protoImport {
			if impprotopkg == fd.GetPackage() {
				continue
			}
			addImport += `    "` + ProtoPkg2GoPkgMap[impprotopkg] + `"
`
		}
		genFile := &plugin_go.CodeGeneratorResponse_File{
			Name:    proto.String(fd.Options.GetGoPackage() + "/" + rpcFilename),
			Content: proto.String(rpcFileHeadContent + addImport + importEnd + rpcFileContent),
		}

		genFiles = append(genFiles, genFile)

	}

	return
}

func genSoapHttpHandler(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenSoapHttpHandler(request)
	genFiles = append(genFiles, rpcGoItems...)

	return
}

type SoapMethod struct {
	ProtoFd      *descriptor.FileDescriptorProto
	Protoservice *descriptor.ServiceDescriptorProto
	ProtoMethod  *descriptor.MethodDescriptorProto
	InMsg        *descriptor.DescriptorProto
	OutMsg       *descriptor.DescriptorProto
}

func (this SoapMethod) ServiceName() string {
	return this.Protoservice.GetName()
}

func (this SoapMethod) MethodName() string {
	return this.ProtoMethod.GetName()
}
func (this SoapMethod) MethodGoName() string {
	return ygen.CamelCase(this.ProtoMethod.GetName())
}

func (this SoapMethod) InMsgName() string {
	return this.InMsg.GetName()
}

func (this SoapMethod) OutMsgName() string {
	return this.OutMsg.GetName()
}

// 生成soap http handler
func YGenSoapHttpHandler(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcCRUDHead := `//Package {{.package}}  generated by ygen-gocrud. DO NOT EDIT.
//source: {{.original_file}}
package {{.package}}

import (
    "context"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"strings"
	"fmt"
	"time"

	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/proto"

	"github.com/gorilla/mux"

	"pdtnmssoap/nms"
`

	serverHandlerTpl := `
func {{.ServiceName}}{{.MethodGoName}}(w http.ResponseWriter, r *http.Request) () {
	// set headers
	nms.SetWriterHeaders(w)

	// read wsdl content from r.Body
	defer r.Body.Close()
	content, _ := ioutil.ReadAll(r.Body)
	//var response *{{.OutMsgName}}
	req:=&{{.InMsgName}}{}
	_,el,err:=nms.XmlDecode(content,"//{{.InMsgXmlName}}")
	if err!=nil{
		//can not found inmsg el
		nms.ParamDecodeErr(w,err.Error())
		return
	}
	req.FromXml(el)

	grpcCon,err:=nms.GetGrpcCon()

	if err!=nil{
		nms.GetGrpcConErr(w,err.Error())
		return
	}

	grpcClient:=New{{.ServiceName}}Client(grpcCon)
	
	cauth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)

	username := ""
	pwd := ""
	if len(cauth) == 2 {
		bauth, err := base64.StdEncoding.DecodeString(cauth[1])
		if err != nil {
			nms.B64DecodeErr(w, err.Error())
			return
		}
		auth := string(bauth)
		authArr := strings.SplitN(auth, ":", 2)

		if len(authArr) == 2 {
			username = authArr[0]
			pwd = authArr[1]
		}
	}
	var SoapSource = &nmscommon.SoapSource{
		Url:              r.URL.String(),
		RemoteAddr:       r.RemoteAddr,
		HttpAuthUsername: username,
		HttpAuthPassword: pwd,
	}
	soapsourcebin, err := proto.Marshal(SoapSource)
	if err != nil {
		//err
		nms.MarshalErr(w, err.Error())
		return
	}
	ctx := metadata.NewOutgoingContext(context.Background(), metadata.Pairs("soap-source-bin", string(soapsourcebin)))


	response,err:=grpcClient.{{.MethodGoName}}(ctx,req)

	if nms.ProxyConfig.Debug{
		fmt.Println(time.Now(),"soap call:",SoapSource,req,response,err)
	}

	if err != nil {
		if response != nil {
			nms.SoapInitGrpcClientErr(w, response.GetFaultInfo(), err.Error())
		} else {
			nms.SoapInitGrpcClientErr(w, "001", err.Error())
		}
		return
	}

	el=response.ToXml("")

	nms.WriteSoapResponse(w,el)
	
	return
}
`
	registryTpl := `// Registry {{.ServiceName}} rpc methods
func SoapRegister{{.ServiceName}} (r *mux.Router) {
	{{range .registryHandlers}}r.HandleFunc("/{{.ServiceName}}/{{.MethodName}}", {{.ServiceName}}{{.MethodGoName}})
{{end}}
}
`

	var fd *descriptor.FileDescriptorProto

	for _, fd = range request.GetProtoFile() {

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			//glog.Info("no gen file:", fd.GetName())
			continue
		}

		if len(fd.Service) == 0 {
			continue
		}

		rpcFilename := goutil.ExtractFilename(fd.GetName()) + ".xhttp.go"
		rpcFileContent := ""

		// 头部信息，package,imports
		rpcCRUDHeadIml := template.Must(template.New("rpcCRUDHead").Parse(rpcCRUDHead))
		rpcCRUDHeadData := map[string]interface{}{
			"original_file": fd.GetName(),
			"package":       fd.GetPackage(),
			"goPkg":         fd.Options.GetGoPackage(),
		}

		var rpcCRUDHeadBytes bytes.Buffer
		if err := rpcCRUDHeadIml.Execute(&rpcCRUDHeadBytes, rpcCRUDHeadData); err != nil {
			glog.Fatal(err)
		}
		rpcCRUDHeadContent := rpcCRUDHeadBytes.String()
		headImports := make([]string, 0)

		// services handler
		serverHandlerIml := template.Must(template.New("serverHandlerTpl").Parse(serverHandlerTpl))
		serverHandlerData := map[string]interface{}{}

		for _, srv := range fd.Service {
			srvComments, err := ygen.GetProtoServiceLeadingComments(fd, srv)
			if err != nil {
				glog.Fatal("can not get comment of srv:", srv.GetName())
			}
			//upOrDown := 1 //default to up
			//if strings.Contains(srvComments, "@down") {
			//	upOrDown = 2 //down service
			//}

			serverHandlerData["srvComments"] = srvComments
			serverHandlerData["srvName"] = ygen.CamelCase(srv.GetName())

			// registry center
			registryIml := template.Must(template.New("registryTpl").Parse(registryTpl))
			registryData := map[string]interface{}{}
			registryData["ServiceName"] = ygen.CamelCase(srv.GetName())
			var registryBytes bytes.Buffer
			registryHandlers := make([]map[string]string, 0)

			for _, method := range srv.Method {
				methodGoName := ygen.CamelCase(method.GetName())
				registryMap := make(map[string]string)
				registryMap["ServiceName"] = srv.GetName()
				registryMap["MethodName"] = method.GetName()
				registryMap["MethodGoName"] = methodGoName
				registryHandlers = append(registryHandlers, registryMap)

				serverHandlerData["ServiceName"] = srv.GetName()
				serverHandlerData["MethodName"] = ygen.CamelCase(method.GetName())

				// 方法genMethodInputGoMsg，本包则不需要前缀
				serverHandlerData["InMsgName"] = genMethodInputGoMsg(method.GetInputType(), fd.GetPackage())
				serverHandlerData["OutMsgName"] = genMethodInputGoMsg(method.GetOutputType(), fd.GetPackage())

				serverHandlerData["InMsgXmlName"] = genMethodInputXmlMsg(method.GetInputType())
				serverHandlerData["MethodGoName"] = methodGoName

				var serverHandlerBytes bytes.Buffer
				if err := serverHandlerIml.Execute(&serverHandlerBytes, serverHandlerData); err != nil {
					glog.Fatal(err)
				}

				rpcFileContent = rpcFileContent + serverHandlerBytes.String()

				// 补全import依赖包
				// 从methods的In/Out参数中读取依赖的包名
				headImports = append(headImports, getPackageImportNames(method)...)
			}

			registryData["registryHandlers"] = registryHandlers
			if err := registryIml.Execute(&registryBytes, registryData); err != nil {
				glog.Fatal(err)
			}
			rpcFileContent = rpcFileContent + registryBytes.String()
		}

		for _, v := range headImports {
			// 忽略当前包
			if fd.GetPackage() == v {
				continue
			}
			rpcCRUDHeadContent += "    \"" + ProtoPkg2GoPkgMap[v] + "\"" + "\n"
		}
		// import 结束括号
		rpcCRUDHeadContent += ")\n"

		genFile := &plugin_go.CodeGeneratorResponse_File{
			Name:    proto.String(fd.Options.GetGoPackage() + "/" + rpcFilename),
			Content: proto.String(rpcCRUDHeadContent + rpcFileContent),
		}

		genFiles = append(genFiles, genFile)

	}

	return
}
