package main

import (
	"bytes"
	"gitlab.com/kicad99/ykit/goutil"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
	"text/template"

	"gitlab.com/kicad99/ykit/ygen"
	"google.golang.org/protobuf/types/descriptorpb"

	"github.com/beevik/etree"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	plugin_go "github.com/golang/protobuf/protoc-gen-go/plugin"

	"google.golang.org/protobuf/types/pluginpb"
)

var request plugin_go.CodeGeneratorRequest

//var allMsgXmlTypes = make(map[string]*etree.Element)

//存放全局 需要导入的url
//var schemaAttrsImportArray []etree.Attr
var schemaAttrsImportedMap = make(map[string]etree.Attr)

//pkg.msg -> msg
//var allMsgTypes = make(map[string]*descriptor.DescriptorProto)

var msgCxx = regexp.MustCompile(`(.+)C\d+`)

func msgPdtName(msgname string) string {
	submatch := msgCxx.FindStringSubmatch(msgname)
	if submatch == nil {
		return msgname
	}

	return submatch[1]
}

func main() {

	log.SetPrefix("ygen-wsdl2: ")
	data, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatalf("error: reading input: %v", err)
	}

	var response plugin_go.CodeGeneratorResponse
	if err := proto.Unmarshal(data, &request); err != nil {
		log.Fatalf("error: parsing input proto: %v", err)
	}

	//必须首先执行
	genWsdl2Files := initAllMsgTypes(&request)

	genWsdl2Files = append(genWsdl2Files, genwsdl2(&request)...)

	for _, f := range genWsdl2Files {
		response.File = append(response.File, f)
	}

	genWsdl11Files := genwsdl11ForSoap12(&request)

	for _, f := range genWsdl11Files {
		response.File = append(response.File, f)
	}

	var SupportedFeatures uint64 = uint64(pluginpb.CodeGeneratorResponse_FEATURE_PROTO3_OPTIONAL)
	response.SupportedFeatures = &SupportedFeatures

	if data, err = proto.Marshal(&response); err != nil {
		log.Fatalf("error: failed to marshal output proto: %v", err)
	}
	if _, err := os.Stdout.Write(data); err != nil {
		log.Fatalf("error: failed to write output proto: %v", err)
	}

}

func genwsdl2(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenWSDL2(request)
	genFiles = append(genFiles, rpcGoItems...)
	return
}

//init all msg Types as element
func initAllMsgTypes(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	var fd *descriptor.FileDescriptorProto
	for _, fd = range request.GetProtoFile() {
		// 调用createXmlTypeFile，xml分包
		genFiles = append(genFiles, createXmlTypeFile(fd)...)
	}
	return
}

func genwsdl11ForSoap12(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenWSDL11Soap12(request)
	genFiles = append(genFiles, rpcGoItems...)

	return
}

// 生成wsdl的service,binding元素
func genServiceUrl(srvName string) string {
	protocol := "http:"
	hostname := "www.test.com"
	return protocol + "//" + hostname + "/" + srvName
}

// 生成接口元素element属性名称 namespace:elementName
func genInterfaceElementName(argvName string) string {
	if strings.HasPrefix(argvName, ".") {
		argvName = strings.TrimPrefix(argvName, ".")
	}
	list := strings.Split(argvName, ".")
	listLen := len(list)
	if listLen == 1 {
		return list[0]
	}

	return strings.Join(list[0:listLen-1], ".") + ":" + list[listLen-1]
}

// 生成wsdl根节点
func genDescription(doc *etree.Document, srv *descriptor.ServiceDescriptorProto, dependencys []string) *etree.Element {
	description := etree.NewElement("description")
	description.Space = "wsdl"
	// 根据依赖的包，生成对应的namespace
	description.Attr = make([]etree.Attr, 0)
	for _, pkgName := range dependencys {
		description.Attr = append(description.Attr, etree.Attr{
			Space: "xmlns",
			Key:   pkgName,
			Value: genServiceUrl(pkgName),
		})
	}
	// 添加标准的命名空间
	description.Attr = append(description.Attr, etree.Attr{
		Space: "xmlns",
		Key:   "wsdl",
		Value: "http://www.w3.org/ns/wsdl",
	})
	description.Attr = append(description.Attr, etree.Attr{
		Space: "xmlns",
		Key:   "xs",
		Value: "http://www.w3.org/2001/XMLSchema",
	})
	description.Attr = append(description.Attr, etree.Attr{
		Space: "xmlns",
		Key:   "wsoap",
		Value: "http://www.w3.org/ns/wsdl/soap",
	})
	description.Attr = append(description.Attr, etree.Attr{
		Space: "xmlns",
		Key:   "soap",
		Value: "http://www.w3.org/2003/05/soap-envelope",
	})
	description.Attr = append(description.Attr, etree.Attr{
		Key:   "targetNamespace",
		Value: genServiceUrl(srv.GetName()),
	})
	description.Attr = append(description.Attr, etree.Attr{
		Space: "xmlns",
		Key:   "tns",
		Value: genServiceUrl(srv.GetName()),
	})
	doc.AddChild(description)

	return description
}

// generate types
func genWsdl2Types(dependencys []string) *etree.Element {
	// 生成服务元素
	types := etree.NewElement("types")
	types.Space = "wsdl"

	for _, pkgName := range dependencys {
		schema := etree.NewElement("schema")
		schema.Space = "xs"
		schema.Attr = make([]etree.Attr, 0)
		schema.Attr = append(schema.Attr, etree.Attr{
			Key:   "targetNamespace",
			Value: genServiceUrl(pkgName),
		})
		schema.Attr = append(schema.Attr, etree.Attr{
			Space: "xmlns",
			Key:   pkgName,
			Value: genServiceUrl(pkgName),
		})

		importEl := etree.NewElement("import")
		importEl.Space = "xs"
		importEl.Attr = make([]etree.Attr, 0)
		importEl.Attr = append(importEl.Attr, etree.Attr{
			Key:   "namespace",
			Value: genServiceUrl(pkgName),
		})
		importEl.Attr = append(importEl.Attr, etree.Attr{
			Key:   "schemaLocation",
			Value: pkgName + ".xsd",
		})

		schema.AddChild(importEl)
		types.AddChild(schema)
	}

	return types
}

// generate service element
func genWsdl2Service(srv *descriptor.ServiceDescriptorProto) *etree.Element {
	// 生成服务元素
	service := etree.NewElement("service")
	service.Space = "wsdl"
	service.Attr = make([]etree.Attr, 0)
	service.Attr = append(service.Attr, etree.Attr{
		Key:   "name",
		Value: srv.GetName(),
	})
	service.Attr = append(service.Attr, etree.Attr{
		Key:   "interface",
		Value: "tns:" + srv.GetName(),
	})

	// endpoint
	endpoint := etree.NewElement("endpoint")
	endpoint.Space = "wsdl"
	endpoint.Attr = make([]etree.Attr, 0)
	endpoint.Attr = append(service.Attr, etree.Attr{
		Key:   "name",
		Value: srv.GetName() + "Endpoint",
	})
	endpoint.Attr = append(service.Attr, etree.Attr{
		Key:   "binding",
		Value: "tns:" + srv.GetName() + "Binding",
	})
	endpoint.Attr = append(service.Attr, etree.Attr{
		Key:   "address",
		Value: genServiceUrl(srv.GetName()),
	})
	service.AddChild(endpoint)

	return service
}

// generate binding element
func genWsdl2Binding(srv *descriptor.ServiceDescriptorProto) *etree.Element {
	// 生成binding元素
	binding := etree.NewElement("binding")
	binding.Space = "wsdl"
	binding.Attr = make([]etree.Attr, 0)
	binding.Attr = append(binding.Attr, etree.Attr{
		Key:   "name",
		Value: srv.GetName() + "Binding",
	})
	binding.Attr = append(binding.Attr, etree.Attr{
		Key:   "interface",
		Value: "tns:" + srv.GetName() + "Interface",
	})
	binding.Attr = append(binding.Attr, etree.Attr{
		Key: "type",
		// Value: "http://www.w3.org/ns/wsdl/http",
		Value: "http://www.w3.org/ns/wsdl/soap",
	})
	binding.Attr = append(binding.Attr, etree.Attr{
		Space: "wsoap",
		Key:   "protocol",
		Value: "http://www.w3.org/2003/05/soap/bindings/HTTP/",
	})

	// operation，services下有几个rpc，就需要生成几个
	for _, meth := range srv.Method {
		operation := etree.NewElement("operation")
		operation.Space = "wsdl"
		operation.Attr = make([]etree.Attr, 0)
		operation.Attr = append(operation.Attr, etree.Attr{
			Key:   "ref",
			Value: "tns:" + meth.GetName(),
		})
		operation.Attr = append(operation.Attr, etree.Attr{
			Space: "wsoap",
			Key:   "mep",
			Value: "http://www.w3.org/2003/05/soap/mep/soap-response",
		})

		binding.AddChild(operation)
	}

	return binding
}

// generate interface element
func genWsdl2Interface(srv *descriptor.ServiceDescriptorProto) *etree.Element {
	// 生成interface接口元素
	Inerface := etree.NewElement("interface")
	Inerface.Space = "wsdl"
	Inerface.Attr = make([]etree.Attr, 0)
	Inerface.Attr = append(Inerface.Attr, etree.Attr{
		Key:   "name",
		Value: srv.GetName() + "Interface",
	})

	// 根据rpc生成operation元素
	for _, meth := range srv.Method {
		operation := etree.NewElement("operation")
		operation.Space = "wsdl"
		operation.Attr = make([]etree.Attr, 0)
		operation.Attr = append(operation.Attr, etree.Attr{
			Key:   "name",
			Value: meth.GetName(),
		})
		operation.Attr = append(operation.Attr, etree.Attr{
			Key:   "pattern",
			Value: "http://www.w3.org/ns/wsdl/in-out",
		})
		operation.Attr = append(operation.Attr, etree.Attr{
			Key:   "style",
			Value: "http://www.w3.org/ns/wsdl/style/iri",
		})
		Inerface.AddChild(operation)

		// operation:in
		input := etree.NewElement("input")
		input.Space = "wsdl"
		input.Attr = make([]etree.Attr, 0)
		input.Attr = append(input.Attr, etree.Attr{
			Key:   "messageLabel",
			Value: "In",
		})
		input.Attr = append(input.Attr, etree.Attr{
			Key:   "element",
			Value: genInterfaceElementName(meth.GetInputType()),
		})
		operation.AddChild(input)

		// operation:output
		output := etree.NewElement("output")
		output.Space = "wsdl"
		output.Attr = make([]etree.Attr, 0)
		output.Attr = append(output.Attr, etree.Attr{
			Key:   "messageLabel",
			Value: "Out",
		})
		output.Attr = append(output.Attr, etree.Attr{
			Key:   "element",
			Value: genInterfaceElementName(meth.GetOutputType()),
		})

		operation.AddChild(output)
	}

	return Inerface
}

func getProtoFileDependency(srv *descriptorpb.ServiceDescriptorProto) []string {
	needImportXsd := make(map[string]string)
	for _, meth := range srv.Method {
		inputPackage := genInterfaceElementName(meth.GetInputType())
		packagesList := strings.Split(inputPackage, ":")
		if needImportXsd[packagesList[0]] == "" {
			needImportXsd[packagesList[0]] = packagesList[0]
		}
		outputPackage := genInterfaceElementName(meth.GetOutputType())
		packagesList = strings.Split(outputPackage, ":")
		if needImportXsd[packagesList[0]] == "" {
			needImportXsd[packagesList[0]] = packagesList[0]
		}
	}

	result := make([]string, 0)
	for _, pkgName := range needImportXsd {
		result = append(result, pkgName)
	}

	return result
}

// 生成wsdl 2描述
func YGenWSDL2(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {

	var fd *descriptor.FileDescriptorProto

	for _, fd = range request.GetProtoFile() {

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			//log.Println("no gen file:", fd.GetName())
			continue
		}
		// needWriteThisFile := false

		for _, srv := range fd.Service {
			// needWriteThisFile = true
			rpcFilename := fd.GetPackage() + srv.GetName() + "2.wsdl"
			// 查找到当前服务的依赖包名
			dependencys := getProtoFileDependency(srv)

			doc := etree.NewDocument()
			doc.CreateProcInst("xml", `version="1.0" encoding="UTF-8"`)
			description := genDescription(doc, srv, dependencys)

			// types
			types := genWsdl2Types(dependencys)
			description.AddChild(types)

			// service
			service := genWsdl2Service(srv)
			description.AddChild(service)

			// binding
			binding := genWsdl2Binding(srv)
			description.AddChild(binding)

			// interface
			Interface := genWsdl2Interface(srv)
			description.AddChild(Interface)

			// if !strings.HasSuffix(rpcFileContent, "\n") {
			// 	rpcFileContent = rpcFileContent + "\n"
			// }

			rpcFileContent, err := doc.WriteToString()
			if err != nil {
				log.Fatalln("xml marshal err:", srv.GetName(), fd.GetName(), err)
			}

			genFile := &plugin_go.CodeGeneratorResponse_File{
				Name:    proto.String(rpcFilename),
				Content: proto.String(rpcFileContent),
			}

			genFiles = append(genFiles, genFile)

		}

	}

	return
}

// 生成wsdl 1.1 for soap 1.2描述
func YGenWSDL11Soap12(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {

	/*var fd *descriptor.FileDescriptorProto

	allMsgTypes := make(map[string]*descriptor.DescriptorProto)

	for _, fd = range request.GetProtoFile() {
		// save all msgs
		for _, msgType := range fd.MessageType {
			oldmsg := allMsgTypes[fd.GetPackage()+"."+msgType.GetName()]
			if oldmsg != nil {
				log.Fatalln("two msg has same name", fd.GetName(), msgType.GetName())
			}
			allMsgTypes[fd.GetPackage()+"."+msgType.GetName()] = msgType

		}

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			log.Println("no gen file:", fd.GetName())
			continue
		}
		// needWriteThisFile := false

		tHead := template.Must(template.New("wsdl2.go_head").Parse(rpcCRUDHead))
		sqlHeadData := map[string]interface{}{
			"original_file": fd.GetName(),
			"package":       fd.GetPackage(),
			"goPkg":         fd.Options.GetGoPackage(),
		}

		var tplBytes bytes.Buffer
		if err := tHead.Execute(&tplBytes, sqlHeadData); err != nil {
			log.Fatal(err)
		}

		//rpcFileContent := tplBytes.String()

		for _, srv := range fd.Service {
			// needWriteThisFile = true

			log.Println("srv:", srv.GetName())
			rpcFilename := fd.GetPackage() + srv.GetName() + ".wsdl"

			doc := etree.NewDocument()
			doc.CreateProcInst("xml", `version="1.0" encoding="UTF-8"`)
			description := genDescription(doc, srv,nil)

			for _, meth := range srv.Method {
				log.Println("meth:", meth.GetName(), meth.GetInputType(), meth.GetOutputType())
			}

			// service
			service := genWsdl2Service(srv)
			description.AddChild(service)

			// binding
			binding := genWsdl2Binding(srv)
			description.AddChild(binding)

			// interface
			Interface := genWsdl2Interface(srv)
			description.AddChild(Interface)

			// todo message

			// todo types

			// if !strings.HasSuffix(rpcFileContent, "\n") {
			// 	rpcFileContent = rpcFileContent + "\n"
			// }

			rpcFileContent, err := doc.WriteToString()
			if err != nil {
				log.Fatalln("xml marshal err:", srv.GetName(), fd.GetName(), err)
			}

			genFile := &plugin_go.CodeGeneratorResponse_File{
				Name:    proto.String(rpcFilename),
				Content: proto.String(rpcFileContent),
			}

			genFiles = append(genFiles, genFile)

		}

	}

	return*/

	//var fd *descriptor.FileDescriptorProto
	//
	//for _, fd = range request.GetProtoFile() {
	//
	//	if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
	//		log.Println("no gen file:", fd.GetName())
	//		continue
	//	}
	//	// needWriteThisFile := false
	//
	//	for _, srv := range fd.Service {
	//		// needWriteThisFile = true
	//		rpcFilename := fd.GetPackage() + srv.GetName() + "2.wsdl"
	//		// 查找到当前服务的依赖包名
	//		dependencys := getProtoFileDependency(srv)
	//
	//		doc := etree.NewDocument()
	//		doc.CreateProcInst("xml", `version="1.0" encoding="UTF-8"`)
	//		description := genDescription(doc, srv, dependencys)
	//
	//		// types
	//		types := genWsdl2Types(dependencys)
	//		description.AddChild(types)
	//
	//		// service
	//		service := genWsdl2Service(srv)
	//		description.AddChild(service)
	//
	//		// binding
	//		binding := genWsdl2Binding(srv)
	//		description.AddChild(binding)
	//
	//		// interface
	//		Interface := genWsdl2Interface(srv)
	//		description.AddChild(Interface)
	//
	//		// if !strings.HasSuffix(rpcFileContent, "\n") {
	//		// 	rpcFileContent = rpcFileContent + "\n"
	//		// }
	//
	//		rpcFileContent, err := doc.WriteToString()
	//		if err != nil {
	//			log.Fatalln("xml marshal err:", srv.GetName(), fd.GetName(), err)
	//		}
	//
	//		genFile := &plugin_go.CodeGeneratorResponse_File{
	//			Name:    proto.String(rpcFilename),
	//			Content: proto.String(rpcFileContent),
	//		}
	//
	//		genFiles = append(genFiles, genFile)
	//
	//	}
	//
	//}

	return
}

func genXmlGoMsg(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenSoapXmlMsg(request)
	genFiles = append(genFiles, rpcGoItems...)

	return
}

// 生成go struct for xml
func YGenSoapXmlMsg(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	var fd *descriptor.FileDescriptorProto

	rpcCRUDHead := `//Package {{.package}}  generated by ygen-gocrud. DO NOT EDIT.
//source: {{.original_file}}
package {{.package}}

import (
    "encoding/xml"
`
	importEnd := ")\n"

	//proto pkgname -> go pkgname
	protopkgname2gopkgname := make(map[string]string)

	for _, fd = range request.GetProtoFile() {
		protopkgname2gopkgname[fd.GetPackage()] = fd.GetOptions().GetGoPackage()

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			log.Println("no gen file:", fd.GetName())
			continue
		}
		if len(fd.MessageType) == 0 {
			continue
		}

		tHead := template.Must(template.New("soap-xml.go_head").Parse(rpcCRUDHead))
		sqlHeadData := map[string]interface{}{
			"original_file": fd.GetName(),
			"package":       fd.GetPackage(),
			"goPkg":         fd.Options.GetGoPackage(),
		}

		var tplBytes bytes.Buffer
		if err := tHead.Execute(&tplBytes, sqlHeadData); err != nil {
			log.Fatal(err)
		}

		rpcFileHeadContent := tplBytes.String()

		additionalImportMap := make(map[string]string)

		rpcFileContent := ""

		rpcFilename := goutil.ExtractFilename(fd.GetName()) + ".xml.go"

		space4 := "    "

		for _, msg := range fd.MessageType {
			// The full type name
			rpcFileContent += "type " + ygen.CamelCase(msg.GetName()) + " struct {\n"
			xmlNameLine := space4 + "XMLName"

			fieldLineMap := make(map[int]string)

			//找字段的最长长度
			fieldLen := 1
			for _, field := range msg.Field {
				if len(field.GetName()) > fieldLen {
					fieldLen = len(field.GetName())
				}
			}

			//加一个空格
			fieldLen += 1
			if fieldLen <= len(xmlNameLine) {
				fieldLen = len(xmlNameLine) + 1
			}

			xmlNameLine += strings.Repeat(" ", fieldLen-len(xmlNameLine))

			//生成 name + type
			for j, field := range msg.Field {
				line := space4 + ygen.CamelCase(field.GetName()) +
					strings.Repeat(" ", fieldLen-len(field.GetName())) + genFieldType(fd, msg, field)
				fieldLineMap[j] = line

				if strings.HasPrefix(field.GetTypeName(), ".") {
					importpkg := strings.Split(field.GetTypeName(), ".")[1]
					additionalImportMap[importpkg] = importpkg
				}

			}

			//补齐空格
			fieldLen = 1
			for _, l := range fieldLineMap {
				if len(l) > fieldLen {
					fieldLen = len(l)
				}
			}
			fieldLen += 1
			if fieldLen <= len(xmlNameLine) {
				fieldLen = len(xmlNameLine) + 1
			}
			for i, l := range fieldLineMap {
				fieldLineMap[i] = l + strings.Repeat(" ", fieldLen-len(l))
			}
			xmlNameLine += "xml.Name"

			xmlNameLine += strings.Repeat(" ", fieldLen-len(xmlNameLine))

			//add xml tag
			for i, l := range fieldLineMap {
				fieldLineMap[i] = l + "`xml:\"" + msg.Field[i].GetName() + ",omitempty\"`" + "\n"
			}

			xmlNameLine += "`xml:\"" + msg.GetName() + "\"`" + "\n"
			rpcFileContent += xmlNameLine

			for i := range msg.Field {
				rpcFileContent += fieldLineMap[i]
			}

			rpcFileContent += "}\n"

		}

		//fix import
		addImport := ""
		for imp, _ := range additionalImportMap {
			addImport += space4 + "\"" + protopkgname2gopkgname[imp] + "\"\n"
		}

		genFile := &plugin_go.CodeGeneratorResponse_File{
			Name:    proto.String(fd.Options.GetGoPackage() + "/" + rpcFilename),
			Content: proto.String(rpcFileHeadContent + addImport + importEnd + rpcFileContent),
		}

		genFiles = append(genFiles, genFile)

	}

	return
}

func genFieldType(fd *descriptor.FileDescriptorProto, message *descriptor.DescriptorProto, field *descriptorpb.FieldDescriptorProto) string {
	//fieldname := field.GetName()
	result := ""
	if ygen.IsFieldRepeated(field) {
		result += "[]"
	}

	if len(field.GetTypeName()) > 0 {
		//特殊类型
		return result + ygen.ProtoType2GolangType(field.GetTypeName())
	} else {
		//内置类型
		result, _ = ygen.FieldGoType(fd, message, field)
	}

	return result

}

func genSoapGrpcService(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenSoapGrpcService(request)
	genFiles = append(genFiles, rpcGoItems...)

	return
}

// 生成grpc service
func YGenSoapGrpcService(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {

	var fd *descriptor.FileDescriptorProto

	allMsgTypes := make(map[string]*descriptor.DescriptorProto)

	for _, fd = range request.GetProtoFile() {
		// save all msgs
		for _, msgType := range fd.MessageType {
			oldmsg := allMsgTypes[fd.GetPackage()+"."+msgType.GetName()]
			if oldmsg != nil {
				log.Fatalln("two msg has same name", fd.GetName(), msgType.GetName())
			}
			allMsgTypes[fd.GetPackage()+"."+msgType.GetName()] = msgType

		}

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			log.Println("no gen file:", fd.GetName())
			continue
		}
		// needWriteThisFile := false

		// tHead := template.Must(template.New("wsdl2.go_head").Parse(rpcCRUDHead))
		// sqlHeadData := map[string]interface{}{
		//	"original_file": fd.GetName(),
		//	"package":       fd.GetPackage(),
		//	"goPkg":         fd.Options.GetGoPackage(),
		// }

		// var tplBytes bytes.Buffer
		// if err := tHead.Execute(&tplBytes, sqlHeadData); err != nil {
		//	log.Fatal(err)
		// }

		// rpcFileContent := tplBytes.String()

		// for _, srv := range fd.Service {
		// 	// needWriteThisFile = true
		//
		// 	log.Println("srv:", srv.GetName())
		// 	rpcFilename := fd.GetPackage() + srv.GetName() + ".wsdl"
		//
		// 	doc := etree.NewDocument()
		// 	doc.CreateProcInst("xml", `version="1.0" encoding="UTF-8"`)
		// 	description := genDescription(doc, srv)
		//
		// 	for _, meth := range srv.Method {
		// 		log.Println("meth:", meth.GetName(), meth.GetInputType(), meth.GetOutputType())
		// 	}
		//
		// 	// service
		// 	service := genWsdl2Service(srv)
		// 	description.AddChild(service)
		//
		// 	// binding
		// 	binding := genWsdl2Binding(srv)
		// 	description.AddChild(binding)
		//
		// 	// interface
		// 	Interface := genWsdl2Interface(srv)
		// 	description.AddChild(Interface)
		//
		// 	// todo message
		//
		// 	// todo types
		//
		// 	// if !strings.HasSuffix(rpcFileContent, "\n") {
		// 	// 	rpcFileContent = rpcFileContent + "\n"
		// 	// }
		//
		// 	rpcFileContent, err := doc.WriteToString()
		// 	if err != nil {
		// 		log.Fatalln("xml marshal err:", srv.GetName(), fd.GetName(), err)
		// 	}
		//
		// 	genFile := &plugin_go.CodeGeneratorResponse_File{
		// 		Name:    proto.String(rpcFilename),
		// 		Content: proto.String(rpcFileContent),
		// 	}
		//
		// 	genFiles = append(genFiles, genFile)
		//
		// }

	}

	return
}

func genSoapHttpHandler(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	rpcGoItems := YGenSoapHttpHandler(request)
	genFiles = append(genFiles, rpcGoItems...)

	return
}

// 生成soap http handler
func YGenSoapHttpHandler(request *plugin_go.CodeGeneratorRequest) (genFiles []*plugin_go.CodeGeneratorResponse_File) {

	rpcCRUDHead := `//Package {{.package}}  generated by ygen-gocrud. DO NOT EDIT.
//source: {{.original_file}}
package {{.package}}

import (
    "net/http"
	"io/ioutil"

	"github.com/gorilla/mux"

	"pdtnmssoap/nms"
)
`

	serverHandlerTpl := `
func {{.operationName}}(w http.ResponseWriter, r *http.Request) () {
	// set headers
	setWriterHeaders(w)

	// read wsdl content from r.Body
	defer r.Body.Close()
	content, _ := ioutil.ReadAll(r.Body)
	log.Println("{{.operationName}} body", string(content))
	
	return
}
`
	registryTpl := `
// set response header key->"Access-Control-Allow-Origin", value->"*"
func setWriterHeader(w http.ResponseWriter, key, value string) {
    w.Header().Set(key, value)
}

func setWriterHeaders(w http.ResponseWriter) {
	setWriterHeader(w, "Access-Control-Allow-Origin", "*")
	setWriterHeader(w, "Content-Type", "text/xml; charset=utf-8")
	setWriterHeader(w, "Accept-Encoding", "gzip")
}

// Service Operation Registry center
func ServiceOperationRegistry(r *mux.Router) {
	{{range .registryHandlers}}{{.}}{{end}}
}
`

	space4 := "    "
	var fd *descriptor.FileDescriptorProto

	for _, fd = range request.GetProtoFile() {

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			log.Println("no gen file:", fd.GetName())
			continue
		}

		if len(fd.Service) == 0 {
			continue
		}

		rpcFilename := goutil.ExtractFilename(fd.GetName()) + ".xhttp.go"
		rpcFileContent := ""

		// 头部信息，package,imports
		rpcCRUDHeadIml := template.Must(template.New("rpcCRUDHead").Parse(rpcCRUDHead))
		rpcCRUDHeadData := map[string]interface{}{
			"original_file": fd.GetName(),
			"package":       fd.GetPackage(),
			"goPkg":         fd.Options.GetGoPackage(),
		}

		var rpcCRUDHeadBytes bytes.Buffer
		if err := rpcCRUDHeadIml.Execute(&rpcCRUDHeadBytes, rpcCRUDHeadData); err != nil {
			log.Fatal(err)
		}

		// services handler
		serverHandlerIml := template.Must(template.New("serverHandlerTpl").Parse(serverHandlerTpl))
		serverHandlerData := map[string]interface{}{}

		// registry center
		registryIml := template.Must(template.New("registryTpl").Parse(registryTpl))
		registryData := map[string]interface{}{}
		var registryBytes bytes.Buffer
		registryHandlers := make([]string, 0)

		for _, srv := range fd.Service {
			srvComments, err := ygen.GetProtoServiceLeadingComments(fd, srv)
			if err != nil {
				log.Fatal("can not get comment of srv:", srv.GetName())
			}
			upOrDown := 1 //default to up
			if strings.Contains(srvComments, "@down") {
				upOrDown = 2 //down service
			}

			serverHandlerData["srvComments"] = srvComments
			serverHandlerData["srvName"] = ygen.CamelCase(srv.GetName())

			for _, method := range srv.Method {
				log.Println("meth:", method.GetName(), upOrDown)
				routeName := "/" + srv.GetName() + "/" + method.GetName()
				operationName := srv.GetName() + ygen.CamelCase(method.GetName())
				registryHandlers = append(registryHandlers, "r.HandleFunc(\""+routeName+"\", "+operationName+")\n"+space4)
				registryData["registryHandlers"] = registryHandlers

				serverHandlerData["operationName"] = operationName
				var serverHandlerBytes bytes.Buffer
				if err := serverHandlerIml.Execute(&serverHandlerBytes, serverHandlerData); err != nil {
					log.Fatal(err)
				}

				rpcFileContent = rpcFileContent + serverHandlerBytes.String()
			}
		}

		if err := registryIml.Execute(&registryBytes, registryData); err != nil {
			log.Fatal(err)
		}
		rpcFileContent = rpcFileContent + registryBytes.String()

		genFile := &plugin_go.CodeGeneratorResponse_File{
			Name:    proto.String(rpcFilename),
			Content: proto.String(rpcCRUDHeadBytes.String() + rpcFileContent),
		}

		genFiles = append(genFiles, genFile)

	}

	return
}

//TYPE_STRING  TYPE_INT32 TYPE_INT64  TYPE_BOOL  TYPE_DOUBLE
//translate types
func getTypeByFullName(fullName string) string {
	typeMap := make(map[string]string)
	typeMap["TYPE_STRING"] = "string"
	typeMap["TYPE_INT32"] = "int"
	typeMap["TYPE_SINT32"] = "int"
	typeMap["TYPE_INT64"] = "long"
	typeMap["TYPE_BOOL"] = "boolean"
	typeMap["TYPE_DOUBLE"] = "double"
	return typeMap[fullName]
}

//check if ComplexType has imported
func checkComplexTypeHasImported(localPkgName, targetPkgName string) {
	if localPkgName != targetPkgName {
		if _, ok := schemaAttrsImportedMap[targetPkgName]; ok {
			return
		}

		//未导入该包
		appenedAttr := etree.Attr{
			Space: "xmlns",
			Key:   targetPkgName,
			Value: genServiceUrl(targetPkgName),
		}

		schemaAttrsImportedMap[targetPkgName] = appenedAttr
	}
}

//create ComplexTypes element
func createComplexTypes(pkgName, msgName string, types []*descriptorpb.FieldDescriptorProto) (complexType *etree.Element) {
	complexType = etree.NewElement("complexType")
	complexType.Space = "xs"
	complexType.Attr = make([]etree.Attr, 1)
	complexType.Attr[0] = etree.Attr{
		Key:   "name",
		Value: "type_" + msgName,
	}

	sequence := etree.NewElement("sequence")
	sequence.Space = "xs"
	for i := range types {
		if types[i].GetName() == "faultInfo" {
			continue
		}
		element := etree.NewElement("element")
		element.Space = "xs"
		element.Attr = make([]etree.Attr, 2)
		var eleName = ""
		if types[i].GetType().String() == "TYPE_MESSAGE" {
			// 是特殊结构类型
			// 找到对应的message，然后递归
			strs := strings.Split(types[i].GetTypeName(), ".")
			eleName = strs[1] + ":type_" + strs[2]
			//校验schema需要import的类型
			checkComplexTypeHasImported(pkgName, strs[1])
		} else {
			//是基本结构类型
			//eleName = "xs" + ":" + getTypeByFullName(types[i].GetType().String())
			eleName = "xs:" + getTypeByFullName(types[i].GetType().String())
		}
		element.Attr[0] = etree.Attr{
			Key:   "name",
			Value: msgPdtName(types[i].GetName()),
		}
		element.Attr[1] = etree.Attr{
			Key:   "type",
			Value: eleName,
		}
		//判断fields 的label 是否为 “LABEL_REPEATED”，是则加上 maxOccurs = “unbounded”
		if types[i].GetLabel().String() == "LABEL_REPEATED" {
			element.Attr = append(element.Attr, etree.Attr{
				Key:   "maxOccurs",
				Value: "unbounded",
			})
		}
		sequence.AddChild(element)
	}
	complexType.AddChild(sequence)
	return
}

/**
文件名
文件句柄
*/
func createXmlTypeFile(fd *descriptor.FileDescriptorProto) (genFiles []*plugin_go.CodeGeneratorResponse_File) {
	var url = genServiceUrl(fd.GetPackage())
	pkgName := fd.GetPackage()

	doc := etree.NewDocument()
	doc.CreateProcInst("xml", `version="1.0" encoding="UTF-8"`)

	schema := etree.NewElement("schema")
	schema.Space = "xs"
	schema.Attr = make([]etree.Attr, 3)

	schema.Attr[0] = etree.Attr{
		Space: "xmlns",
		Key:   "xs",
		Value: "http://www.w3.org/2001/XMLSchema",
	}
	schema.Attr[1] = etree.Attr{
		Space: "xmlns",
		Key:   pkgName,
		Value: url,
	}
	schema.Attr[2] = etree.Attr{
		Key:   "targetNamespace",
		Value: url,
	}

	//createComplexTypes
	for i := range fd.MessageType {
		var fields = fd.MessageType[i].GetField()
		var name = msgPdtName(fd.MessageType[i].GetName())
		//log.Println("fd.MessageType:", fields)
		complexType := createComplexTypes(pkgName, fd.MessageType[i].GetName(), fields)

		if fd.GetPackage() != "nmscommon" {
			//xs:element
			elem := etree.NewElement("element")
			elem.Space = "xs"
			elem.Attr = make([]etree.Attr, 2)
			elem.Attr[0] = etree.Attr{
				Key:   "name",
				Value: name,
			}
			elem.Attr[1] = etree.Attr{
				Key:   "type",
				Value: "type_" + fd.MessageType[i].GetName(),
			}

			schema.AddChild(elem)
		}
		schema.AddChild(complexType)
	}

	//引入所有的import
	for s := range schemaAttrsImportedMap {
		schema.Attr = append(schema.Attr, schemaAttrsImportedMap[s])
	}
	doc.AddChild(schema)

	rpcFileContent, err := doc.WriteToString()
	if err != nil {
		log.Fatalln("xml marshal err:", fd.GetName(), err)
	}

	genFile := &plugin_go.CodeGeneratorResponse_File{
		Name:    proto.String(pkgName + ".xsd"),
		Content: proto.String(rpcFileContent),
	}

	genFiles = append(genFiles, genFile)
	return
}
