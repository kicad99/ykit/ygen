package main

import (
	"io/ioutil"
	"log"
	"strings"

	"github.com/gogo/protobuf/proto"
	"github.com/gogo/protobuf/protoc-gen-gogo/descriptor"
	"github.com/gogo/protobuf/protoc-gen-gogo/generator"
	plugin "github.com/gogo/protobuf/protoc-gen-gogo/plugin"
	"github.com/gogo/protobuf/vanity"
	"github.com/gogo/protobuf/vanity/command"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/ygen"
)

func main() {
	g := generator.New()
	data, err := ioutil.ReadFile("/tmp/ytmp/ttt.ttt")
	if err != nil {
		g.Error(err, "reading input")
	}

	if err := proto.Unmarshal(data, g.Request); err != nil {
		g.Error(err, "parsing input proto")
	}

	if len(g.Request.FileToGenerate) == 0 {
		g.Fail("no files to generate")
	}
	req := g.Request
	files := req.GetProtoFile()
	files = vanity.FilterFiles(files, vanity.NotGoogleProtobufDescriptorProto)

	vanity.ForEachFile(files, vanity.TurnOnMarshalerAll)
	vanity.ForEachFile(files, vanity.TurnOnSizerAll)
	vanity.ForEachFile(files, vanity.TurnOnUnmarshalerAll)

	vanity.ForEachFieldInFilesExcludingExtensions(vanity.OnlyProto2(files), vanity.TurnOffNullableForNativeTypesWithoutDefaultsOnly)
	vanity.ForEachFile(files, vanity.TurnOffGoUnrecognizedAll)
	vanity.ForEachFile(files, vanity.TurnOffGoUnkeyedAll)
	vanity.ForEachFile(files, vanity.TurnOffGoSizecacheAll)

	resp := command.Generate(req)

	resp = ykitPatch(req, resp)

	command.Write(resp)
}

//patch resp from gogofaster to add ykit tag in generated go struct
func ykitPatch(req *plugin.CodeGeneratorRequest, resp *plugin.CodeGeneratorResponse) *plugin.CodeGeneratorResponse {
	reqFiles := req.GetProtoFile()
	for _, reqf := range reqFiles {
		respfilename := goutil.ExtractFilename(reqf.GetName()) + ".pb.go"
		for _, respf := range resp.GetFile() {
			if strings.Contains(*respf.Name, respfilename) {
				ykitPatchFile(reqf, respf)
			}
		}
	}
	return resp
}

func ykitPatchFile(reqf *descriptor.FileDescriptorProto, respf *plugin.CodeGeneratorResponse_File) {
	result := strings.Builder{}
	resultPos := 0
	origResult := *respf.Content

	for _, msg := range reqf.MessageType {
		for _, field := range msg.Field {
			ykitTag := genYkitTag(reqf, msg, field)

			if len(ykitTag) > 0 {
				msgStruct := "type " + *msg.Name + " struct"
				msgStructPos := strings.Index(origResult, msgStruct)
				if msgStructPos < 0 {
					log.Fatal("can not find msg struct:", msgStruct, *respf.Name)
				}
				fieldJsonTag := *field.Name + ",omitempty"

				tagPos := goutil.StrIndex(origResult, fieldJsonTag, msgStructPos)
				if tagPos < 0 {
					log.Fatal("can not get json tag:", *msg.Name, fieldJsonTag)
				}
				newPos := tagPos + len(fieldJsonTag) + 1
				result.WriteString(origResult[resultPos:newPos])
				result.WriteString(ykitTag)
				resultPos = newPos
			}
		}
	}

	result.WriteString(origResult[resultPos:])

	r := result.String()
	//log.Println("new file:", r)
	respf.Content = &r
}

func genYkitTag(reqf *descriptor.FileDescriptorProto, msg *descriptor.DescriptorProto, field *descriptor.FieldDescriptorProto) string {
	_, filedComment := ygen.GetProtoMsgFieldLeadingCommentsgogo(reqf, msg, field)

	return ygen.GenYkitTag(filedComment)
}
