package main

import (
	"strings"

	"github.com/gogo/protobuf/protoc-gen-gogo/descriptor"
	plugin "github.com/gogo/protobuf/protoc-gen-gogo/plugin"
	"github.com/gogo/protobuf/vanity"
	"github.com/gogo/protobuf/vanity/command"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/ygen"
)

func main() {
	req := command.Read()
	//data, _ := proto.Marshal(req)
	//_ = ioutil.WriteFile("/tmp/ytmp/ttt.ttt", data, 0777)
	files := req.GetProtoFile()
	files = vanity.FilterFiles(files, vanity.NotGoogleProtobufDescriptorProto)

	vanity.ForEachFile(files, vanity.TurnOnMarshalerAll)
	vanity.ForEachFile(files, vanity.TurnOnSizerAll)
	vanity.ForEachFile(files, vanity.TurnOnUnmarshalerAll)

	vanity.ForEachFieldInFilesExcludingExtensions(vanity.OnlyProto2(files), vanity.TurnOffNullableForNativeTypesWithoutDefaultsOnly)
	vanity.ForEachFile(files, vanity.TurnOffGoUnrecognizedAll)
	vanity.ForEachFile(files, vanity.TurnOffGoUnkeyedAll)
	vanity.ForEachFile(files, vanity.TurnOffGoSizecacheAll)

	resp := command.Generate(req)

	resp = ykitPatch(req, resp)

	command.Write(resp)
}

//patch resp from gogofaster to add ykit tag in generated go struct
func ykitPatch(req *plugin.CodeGeneratorRequest, resp *plugin.CodeGeneratorResponse) *plugin.CodeGeneratorResponse {
	reqFiles := req.GetProtoFile()
	for _, reqf := range reqFiles {
		respfilename := goutil.ExtractFilename(reqf.GetName()) + ".pb.go"
		for _, respf := range resp.GetFile() {
			if strings.Contains(*respf.Name, respfilename) {
				ykitPatchFile(reqf, respf)
			}
		}
	}
	return resp
}

func ykitPatchFile(reqf *descriptor.FileDescriptorProto, respf *plugin.CodeGeneratorResponse_File) {
	result := strings.Builder{}
	resultPos := 0
	origResult := *respf.Content

	if len(origResult) == 0 {
		return
	}

	for _, msg := range reqf.MessageType {
		if !strings.HasPrefix(*msg.Name, "Db") {
			//如果不是以Db开头则不处理，规定数据库相关的表message必须以Db开头
			continue
		}
		for _, field := range msg.Field {
			ykitTag := genYkitTag(reqf, msg, field)

			if len(ykitTag) > 0 {
				msgStruct := "type " + *msg.Name + " struct"
				msgStructPos := strings.Index(origResult, msgStruct)
				if msgStructPos < 0 {
					//log.Fatal("can not find msg struct:", msgStruct, *respf.Name)
					continue
				}
				fieldJsonTag := *field.Name + ",omitempty"

				tagPos := goutil.StrIndex(origResult, fieldJsonTag, msgStructPos)
				if tagPos < 0 {
					//log.Fatal("can not get json tag:", *msg.Name, fieldJsonTag)
					continue
				}
				newPos := tagPos + len(fieldJsonTag) + 1
				result.WriteString(origResult[resultPos:newPos])
				result.WriteString(ykitTag)
				resultPos = newPos
			}
		}
	}

	result.WriteString(origResult[resultPos:])

	r := result.String()
	//log.Println("new file:", r)
	respf.Content = &r
}

func genYkitTag(reqf *descriptor.FileDescriptorProto, msg *descriptor.DescriptorProto, field *descriptor.FieldDescriptorProto) string {
	_, filedComment := ygen.GetProtoMsgFieldLeadingCommentsgogo(reqf, msg, field)

	return ygen.GenYkitTag(filedComment)
}
