#!/bin/sh

cd "$(dirname "$0")"

./build.sh
protoc --ygen-gogofaster_out=/tmp/ytmp test.proto
