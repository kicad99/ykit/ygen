module gitlab.com/kicad99/ykit/ygen

go 1.13

require (
	github.com/beevik/etree v1.1.1-0.20200718192613-4a2f8b9d084c
	github.com/gogo/protobuf v1.3.2
	github.com/golang/glog v1.0.0
	github.com/golang/protobuf v1.4.3
	gitlab.com/kicad99/ykit/goutil v1.14.0
	google.golang.org/protobuf v1.25.0
)
