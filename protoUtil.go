package ygen

import (
	"errors"
	"strings"
	"unicode"

	"github.com/golang/glog"

	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
	"gitlab.com/kicad99/ykit/goutil"
)

// 得到 proto文件中 Message的前导注释
func GetProtoMsgLeadingComments(fd *descriptor.FileDescriptorProto, msg *descriptor.DescriptorProto) string {

	locs := fd.SourceCodeInfo.Location
	msgs := fd.MessageType

	if locs == nil {
		return ""
	}

	msgIdx := -1

	//find msg idx
	for idx, msgItem := range msgs {
		if msgItem == msg {
			msgIdx = idx
			break
		}
	}

	if msgIdx < 0 {
		//not found the msg
		return ""
	}

	for _, loc := range locs {
		path := loc.Path

		if len(path) == 2 &&
			path[0] == 4 &&
			path[1] == int32(msgIdx) {
			if loc.LeadingComments != nil {
				return *loc.LeadingComments
			} else {
				break
			}
		}
	}

	return ""
}

// 得到 progo message中字段的前导注释
func GetProtoMsgFieldLeadingComments(fd *descriptor.FileDescriptorProto, msg *descriptor.DescriptorProto, field *descriptor.FieldDescriptorProto) (error, string) {
	locs := fd.SourceCodeInfo.Location
	msgs := fd.MessageType
	fields := msg.Field

	if locs == nil {
		return nil, ""
	}

	msgIdx := -1

	//find msg idx
	for idx, msgItem := range msgs {
		if msgItem == msg {
			msgIdx = idx
			break
		}
	}

	if msgIdx < 0 {
		return errors.New("not found msg:" + *msg.Name), ""
	}

	fieldIdx := -1
	for idx, fieldItem := range fields {
		if fieldItem == field {
			fieldIdx = idx
			break
		}
	}

	if fieldIdx < 0 {
		return errors.New("not found field:" + *field.Name), ""
	}

	for _, loc := range locs {
		path := loc.Path

		if len(path) == 4 &&
			path[0] == 4 &&
			path[1] == int32(msgIdx) &&
			path[2] == 2 &&
			path[3] == int32(fieldIdx) {
			if loc.LeadingComments != nil {
				return nil, *loc.LeadingComments
			} else {
				break
			}
		}
	}

	return nil, ""
}

// Is this field repeated?
func IsFieldRepeated(field *descriptor.FieldDescriptorProto) bool {
	return field.Label != nil && *field.Label == descriptor.FieldDescriptorProto_LABEL_REPEATED
}

// Is this field optional?
func IsFieldOptional(field *descriptor.FieldDescriptorProto) bool {
	return field.Label != nil && *field.Label == descriptor.FieldDescriptorProto_LABEL_OPTIONAL && field.OneofIndex != nil
}

// generate ykit go struct tag for protobuf message field
func GenYkitTag(filedComment string) string {

	if len(filedComment) == 0 {
		return ""
	}

	db, _ := ExtractFieldLevelSql(filedComment)

	if len(db) == 0 {
		return ` ykit:"nodb"`
	}

	ykitTags := make([]string, 0)
	dbStr := strings.Join(db, " ")
	dbStr = strings.ToLower(dbStr)
	isGenerated := false
	if strings.Contains(dbStr, "generated") {
		ykitTags = append(ykitTags, "generated")
		isGenerated = true
	}
	if strings.Contains(dbStr, "unique") {
		ykitTags = append(ykitTags, "unique")
	}
	if strings.Contains(dbStr, "primary") {
		ykitTags = append(ykitTags, "prk")
	}
	if strings.Contains(dbStr, "not null") {
		ykitTags = append(ykitTags, "notnull")

	} else {
		if !strings.Contains(dbStr, "primary") && !isGenerated && !strings.Contains(dbStr, "default") {
			ykitTags = append(ykitTags, "null")

		}
	}

	if len(ykitTags) == 0 {
		return ""
	}

	return ` ykit:"` + strings.Join(ykitTags, ",") + `"`

}

func IsProtoFileNeedGenerate(filename string, fileToGenerate []string) bool {
	if goutil.StrSliceIndex(fileToGenerate, filename) < 0 {
		return false
	}
	return true
}

func ProtoPackageName(d *descriptor.FileDescriptorProto) (name string) {

	// Does the file have a package clause?
	if pkg := d.GetPackage(); pkg != "" {
		return pkg
	}
	// Use the file base name.
	return baseName(d.GetName())
}

// baseName returns the last path element of the name, with the last dotted suffix removed.
func baseName(name string) string {
	// First, find the last element
	if i := strings.LastIndex(name, "/"); i >= 0 {
		name = name[i+1:]
	}
	// Now drop the suffix
	if i := strings.LastIndex(name, "."); i >= 0 {
		name = name[0:i]
	}
	return name
}

func GetProtoServiceLeadingComments(fd *descriptor.FileDescriptorProto, service *descriptor.ServiceDescriptorProto) (string, error) {

	locs := fd.SourceCodeInfo.Location
	srvs := fd.Service

	if locs == nil {
		return "", nil
	}

	srvIdx := -1

	//find msg idx
	for idx, srvItem := range srvs {
		if srvItem == service {
			srvIdx = idx
			break
		}
	}

	if srvIdx < 0 {
		return "", errors.New("not found service:" + *service.Name)
	}

	for _, loc := range locs {
		path := loc.Path

		if len(path) == 2 &&
			path[0] == 6 &&
			path[1] == int32(srvIdx) {
			if loc.LeadingComments != nil {
				return *loc.LeadingComments, nil
			} else {
				break
			}
		}
	}

	return "", nil
}
func GetProtoRpcLeadingComments(fd *descriptor.FileDescriptorProto, service *descriptor.ServiceDescriptorProto, method *descriptor.MethodDescriptorProto) (error, string) {

	locs := fd.SourceCodeInfo.Location
	srvs := fd.Service
	methods := service.Method

	if locs == nil {
		return nil, ""
	}

	srvIdx := -1

	//find msg idx
	for idx, srvItem := range srvs {
		if srvItem == service {
			srvIdx = idx
			break
		}
	}

	if srvIdx < 0 {
		return errors.New("not found service:" + *service.Name), ""
	}

	methodIdx := -1
	for idx, fieldItem := range methods {
		if fieldItem == method {
			methodIdx = idx
			break
		}
	}

	if methodIdx < 0 {
		return errors.New("not found method:" + *method.Name), ""
	}

	for _, loc := range locs {
		path := loc.Path

		if len(path) == 4 &&
			path[0] == 6 &&
			path[1] == int32(srvIdx) &&
			path[2] == 2 &&
			path[3] == int32(methodIdx) {
			if loc.LeadingComments != nil {
				return nil, *loc.LeadingComments
			} else {
				break
			}
		}
	}

	return nil, ""
}

func GetProtoMsgFileName(request *plugin.CodeGeneratorRequest, pkgName string, msgName string) string {
	for _, fd := range request.GetProtoFile() {

		if ProtoPackageName(fd) != pkgName {
			continue
		}

		for _, msg := range fd.MessageType {
			if msgName == msg.GetName() {
				return goutil.ExtractFilename(fd.GetName())
			}
		}
	}

	return ""
}

//CamelCase return CamelCase format string, golang need it for export
func CamelCase(name string) string {
	if len(name) == 0 {
		return ""
	}
	r := make([]rune, 0, 32)

	needUpper := false
	for idx, c := range name {
		if idx == 0 {
			r = append(r, unicode.ToUpper(c))
			continue
		}
		if c == '_' || c == '.' {
			needUpper = true
			continue
		}
		if needUpper {
			if unicode.IsDigit(c) {
				r = append(r, c)
			} else {
				r = append(r, unicode.ToUpper(c))

			}
			needUpper = false
		} else {
			r = append(r, c)
		}

	}

	return string(r)

}

//.bbb.abc -> bbb.Abc
func ProtoType2GolangType(ptype string) string {
	if !strings.HasPrefix(ptype, ".") {
		return CamelCase(ptype)
	}

	split := strings.Split(ptype, ".")[1:]

	split[len(split)-1] = CamelCase(split[len(split)-1])
	return strings.Join(split, ".")

}

// FieldGoType returns a string representing the type name, and the wire type
func FieldGoType(fd *descriptor.FileDescriptorProto, message *descriptor.DescriptorProto, field *descriptor.FieldDescriptorProto) (typ string, wire string) {
	// TODO: Options.
	switch *field.Type {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		typ, wire = "float64", "fixed64"
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		typ, wire = "float32", "fixed32"
	case descriptor.FieldDescriptorProto_TYPE_INT64:
		typ, wire = "int64", "varint"
	case descriptor.FieldDescriptorProto_TYPE_UINT64:
		typ, wire = "uint64", "varint"
	case descriptor.FieldDescriptorProto_TYPE_INT32:
		typ, wire = "int32", "varint"
	case descriptor.FieldDescriptorProto_TYPE_UINT32:
		typ, wire = "uint32", "varint"
	case descriptor.FieldDescriptorProto_TYPE_FIXED64:
		typ, wire = "uint64", "fixed64"
	case descriptor.FieldDescriptorProto_TYPE_FIXED32:
		typ, wire = "uint32", "fixed32"
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		typ, wire = "bool", "varint"
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		typ, wire = "string", "bytes"
	//case descriptor.FieldDescriptorProto_TYPE_GROUP:
	//	desc := g.ObjectNamed(field.GetTypeName())
	//	typ, wire = "*"+g.TypeName(desc), "group"
	case descriptor.FieldDescriptorProto_TYPE_MESSAGE:
		//	desc := g.ObjectNamed(field.GetTypeName())
		typename := field.GetTypeName()
		if strings.HasPrefix(typename, ".") {
			slist := strings.Split(typename, ".")
			slist[len(slist)-1] = CamelCase(slist[len(slist)-1])
			if fd.GetPackage() == slist[1] {
				typ, wire = slist[len(slist)-1], "bytes"
			} else {
				typ, wire = strings.Join(slist[1:], "."), "bytes"
			}
		} else {
			typ, wire = CamelCase(typename), "bytes"
		}
	//	typ, wire = "*"+g.TypeName(desc), "bytes"
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		typ, wire = "[]byte", "bytes"
	//case descriptor.FieldDescriptorProto_TYPE_ENUM:
	//	desc := g.ObjectNamed(field.GetTypeName())
	//	typ, wire = g.TypeName(desc), "varint"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED32:
		typ, wire = "int32", "fixed32"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED64:
		typ, wire = "int64", "fixed64"
	case descriptor.FieldDescriptorProto_TYPE_SINT32:
		typ, wire = "int32", "zigzag32"
	case descriptor.FieldDescriptorProto_TYPE_SINT64:
		typ, wire = "int64", "zigzag64"
	default:
		glog.Fatal("unknown type for", field.GetName())
	}
	if IsFieldRepeated(field) {
		if field.GetType() == descriptor.FieldDescriptorProto_TYPE_MESSAGE {
			typ = "[]*" + typ

		} else {
			typ = "[]" + typ

		}
	} else if message != nil && ProtoFileIsProto3(fd) {
		return
	} else if field.OneofIndex != nil && message != nil {
		return
	} else if FieldNeedsStar(*field.Type) {
		typ = "*" + typ
	}
	return
}

func FieldElementGoType(fd *descriptor.FileDescriptorProto, message *descriptor.DescriptorProto, field *descriptor.FieldDescriptorProto) (typ string, wire string) {
	// TODO: Options.
	switch *field.Type {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		typ, wire = "float64", "fixed64"
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		typ, wire = "float32", "fixed32"
	case descriptor.FieldDescriptorProto_TYPE_INT64:
		typ, wire = "int64", "varint"
	case descriptor.FieldDescriptorProto_TYPE_UINT64:
		typ, wire = "uint64", "varint"
	case descriptor.FieldDescriptorProto_TYPE_INT32:
		typ, wire = "int32", "varint"
	case descriptor.FieldDescriptorProto_TYPE_UINT32:
		typ, wire = "uint32", "varint"
	case descriptor.FieldDescriptorProto_TYPE_FIXED64:
		typ, wire = "uint64", "fixed64"
	case descriptor.FieldDescriptorProto_TYPE_FIXED32:
		typ, wire = "uint32", "fixed32"
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		typ, wire = "bool", "varint"
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		typ, wire = "string", "bytes"
	//case descriptor.FieldDescriptorProto_TYPE_GROUP:
	//	desc := g.ObjectNamed(field.GetTypeName())
	//	typ, wire = "*"+g.TypeName(desc), "group"
	case descriptor.FieldDescriptorProto_TYPE_MESSAGE:
		//	desc := g.ObjectNamed(field.GetTypeName())
		typename := field.GetTypeName()
		if strings.HasPrefix(typename, ".") {
			slist := strings.Split(typename, ".")
			slist[len(slist)-1] = CamelCase(slist[len(slist)-1])
			typ, wire = strings.Join(slist[1:], "."), "bytes"
		} else {
			typ, wire = CamelCase(typename), "bytes"
		}
	//	typ, wire = "*"+g.TypeName(desc), "bytes"
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		typ, wire = "[]byte", "bytes"
	//case descriptor.FieldDescriptorProto_TYPE_ENUM:
	//	desc := g.ObjectNamed(field.GetTypeName())
	//	typ, wire = g.TypeName(desc), "varint"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED32:
		typ, wire = "int32", "fixed32"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED64:
		typ, wire = "int64", "fixed64"
	case descriptor.FieldDescriptorProto_TYPE_SINT32:
		typ, wire = "int32", "zigzag32"
	case descriptor.FieldDescriptorProto_TYPE_SINT64:
		typ, wire = "int64", "zigzag64"
	default:
		glog.Fatal("unknown element type for", field.GetName())
	}

	return
}

func ProtoFileIsProto3(file *descriptor.FileDescriptorProto) bool {
	return file.GetSyntax() == "proto3"
}
func FieldNeedsStar(typ descriptor.FieldDescriptorProto_Type) bool {
	switch typ {
	case descriptor.FieldDescriptorProto_TYPE_GROUP:
		return false
	case descriptor.FieldDescriptorProto_TYPE_MESSAGE:
		return false
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		return false
	}
	return true
}

func GetPackageFromMsgName(msgName string) string {
	msgName = strings.TrimPrefix(msgName, ".")
	list := strings.Split(msgName, ".")
	length := len(list)

	return strings.Join(list[:length-1], ".")
}
