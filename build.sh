#!/bin/sh

cd "$(dirname "$0")"

./cmd/protoc-gen-ygen-go/build.sh

./cmd/protoc-gen-ygen-gocrud/build.sh

./cmd/protoc-gen-ygen-gocrud-proto/build.sh

./cmd/protoc-gen-ygen-gogofaster/build.sh

./cmd/protoc-gen-ygen-jsyrpc/build.sh

./cmd/protoc-gen-ygen-sql/build.sh

./cmd/protoc-gen-ygen-wsdl2/build.sh

#./cmd/protoc/build.sh

