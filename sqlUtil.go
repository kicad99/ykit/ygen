package ygen

import (
	"strconv"
	"strings"
)

//MakeDollarList return result like ($1,$2,...,$count)
func MakeDollarList(count int, with_brace bool) (r string) {

	tmp := strings.Builder{}
	if with_brace {
		tmp.WriteString("(")
	}
	for i := 0; i < count; i++ {
		if i != 0 {
			tmp.WriteString(",")
		}
		tmp.WriteString("$")
		tmp.WriteString(strconv.Itoa(i + 1))
	}

	if with_brace {
		tmp.WriteString(")")
	}
	return tmp.String()
}

//提取msg前导注释里面的指令内容,@dbpre,@db,@dbend,@dbpost,comment
func ExtractMsgLevelSql(msgLeadingComment string) (dbpre []string, db []string, dbend []string, dbpost []string, comments []string) {
	commentLines := strings.Split(msgLeadingComment, "\n")

	for _, lineComment := range commentLines {
		lineText := strings.TrimSpace(lineComment)
		if len(lineText) == 0 {
			continue
		}
		if strings.HasPrefix(lineText, "@dbpre") {
			dbpre = append(dbpre, lineText[6:])
			continue
		}

		if strings.HasPrefix(lineText, "@dbpost") {
			dbpost = append(dbpost, lineText[7:])
			continue
		}

		if strings.HasPrefix(lineText, "@dbend") {
			dbend = append(dbend, lineText[6:])
			continue
		}

		if strings.HasPrefix(lineText, "@db") {
			db = append(db, lineText[3:])
			continue
		}

		if strings.HasPrefix(lineText, "@") {
			//@开头的是我们规定的指令，不要作为注释输出
			continue
		}

		if strings.HasPrefix(lineText, "//") {
			comments = append(comments, lineText[2:])
		} else {
			comments = append(comments, lineText)
		}

	}
	return
}

//提取proto msg字段定义注释里面的@db 内容
func ExtractFieldLevelSql(fieldLeadingComment string) (db []string, comments []string) {
	commentLines := strings.Split(fieldLeadingComment, "\n")

	for _, lineComment := range commentLines {
		lineText := strings.TrimSpace(lineComment)

		if len(lineText) == 0 {
			continue
		}

		if strings.HasPrefix(lineText, "@db") {
			db = append(db, lineText[3:])
			continue
		}

		if strings.HasPrefix(lineText, "@") {
			//@开头的是我们规定的指令，不要作为注释输出
			continue
		}

		if strings.HasPrefix(lineText, "//") {
			comments = append(comments, lineText[2:])
		} else {
			comments = append(comments, lineText)
		}

	}
	return
}

//提取@rpc里面的内容
func ExtractMsgRpc(msgLeadingComment string) (rpc []string) {
	commentLines := strings.Split(msgLeadingComment, "\n")

	for _, lineComment := range commentLines {
		lineText := strings.TrimSpace(lineComment)
		if len(lineText) == 0 {
			continue
		}
		if strings.HasPrefix(lineText, "@rpc") {
			rpcStr := lineText[4:]
			rpcs := strings.Split(rpcStr, " ")
			rpc = append(rpc, rpcs...)
			continue
		}
	}
	for i, s := range rpc {
		ss := strings.ToLower(s)
		ss = strings.TrimSpace(ss)
		rpc[i] = ss
	}
	return
}
