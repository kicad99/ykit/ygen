package jsyrpc

import (
	"bytes"
	"log"
	"strconv"
	"strings"
	"text/template"

	"github.com/golang/protobuf/proto"
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
	"gitlab.com/kicad99/ykit/goutil"
	"gitlab.com/kicad99/ykit/ygen"
)

var jsYrpcHead = `import {Writer} from 'protobufjs'
import {rpcCon} from 'jsyrpc'
import {ICallOption} from 'jsyrpc'
`
var jsYrpcHeadRpcStream = `
import {TRpcStream} from 'jsyrpc'
`

//import * as y from '@ygen'

var jsYrpcClass = `
{{$className := .className}}{{ range $no,$rpc := .unaryNocareCalls }}export function {{$className}}{{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}}):boolean{
	let w:Writer={{$rpc.GetInputType}}.encode(req)
	let reqData=w.finish()
	
	const api='{{$rpc.Api}}'
	const v={{$rpc.Version}}

	return rpcCon.NocareCall(reqData,api,v)	
}{{ end }}
{{ range $no,$rpc := .unaryCalls }}export function {{$className}}{{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}},callOpt?:ICallOption):boolean{
	let w:Writer={{$rpc.GetInputType}}.encode(req)
	let reqData=w.finish()
	
	const api='{{$rpc.Api}}'
	const v={{$rpc.Version}}

	return rpcCon.UnaryCall(reqData,api,v,{{$rpc.GetOutputType}},callOpt)
}{{ end }}
{{ range $no,$rpc := .clientStreamCalls }}export function {{$className}}{{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}},callOpt?:ICallOption):TRpcStream{
	const api='{{$rpc.Api}}'
	const v={{$rpc.Version}}
	
	let r=new TRpcStream(api,v,3,{{$rpc.GetInputType}},{{$rpc.GetOutputType}},callOpt)
	r.sendFirst(req)
	return r
}{{ end }}
{{ range $no,$rpc := .serverStreamCalls }}export function {{$className}}{{$rpc.MethodName}}(req:{{$rpc.GetInputType}},callOpt?:ICallOption):TRpcStream{
	const api='{{$rpc.Api}}'
	const v={{$rpc.Version}}
	let r=new TRpcStream(api,v,7,{{$rpc.GetInputType}},{{$rpc.GetOutputType}},callOpt)
	r.sendFirst(req)
	return r
}{{ end }}
{{ range $no,$rpc := .bidiStreamCalls }}export function {{$className}}{{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}},callOpt?:ICallOption):TRpcStream{
	const api='{{$rpc.Api}}'
	const v={{$rpc.Version}}
	let r=new TRpcStream(api,v,8,{{$rpc.GetInputType}},{{$rpc.GetOutputType}},callOpt)
	r.sendFirst(req)
	return r
}{{ end }}
export class {{$className}} { {{ range $no,$rpc := .unaryNocareCalls }}
  public static {{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}}):boolean{
	return {{$className}}{{$rpc.MethodName}}(req)	
}{{ end }}{{ range $no,$rpc := .unaryCalls }}
 public static {{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}},callOpt?:ICallOption):boolean{
	return {{$className}}{{$rpc.MethodName}}(req, callOpt)	
}{{ end }}{{ range $no,$rpc := .clientStreamCalls }}
 public static {{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}},callOpt?:ICallOption):TRpcStream{
	return {{$className}}{{$rpc.MethodName}}(req, callOpt)
}{{ end }}{{ range $no,$rpc := .serverStreamCalls }}
 public static {{$rpc.MethodName}}(req:{{$rpc.GetInputType}},callOpt?:ICallOption):TRpcStream{
	return {{$className}}{{$rpc.MethodName}}(req, callOpt)
}{{ end }}{{ range $no,$rpc := .bidiStreamCalls }}
 public static {{$rpc.MethodName}}(req:{{$rpc.GetInputTypeInterface}},callOpt?:ICallOption):TRpcStream{
	return {{$className}}{{$rpc.MethodName}}(req, callOpt)
}{{ end }}
}`

type TrpcItem struct {
	PkgName            string
	ServiceName        string
	MethodName         string
	MethodComment      string
	InputType          string
	InputTypeFilename  string
	OutputType         string
	OutputTypeFilename string
	// Identifies if client streams multiple client messages
	ClientStreaming bool
	// Identifies if server streams multiple server messages
	ServerStreaming bool
	Version         int
}

func (this *TrpcItem) JsyrpcFunc() string {
	return this.ServiceName + this.MethodName
}
func (this *TrpcItem) Api() string {
	api := "/" + this.PkgName + "." + this.ServiceName + "/" + this.MethodName
	//if this.Version > 0 {
	//	api += "/v"
	//	api += strconv.Itoa(this.Version)
	//}

	return api
}
func (this *TrpcItem) GetInputTypeImport() string {
	return "import * as " + ygen.CamelCase(this.InputTypeFilename) + "Y from '@ygen/" + this.InputTypeFilename + "'"
}
func (this *TrpcItem) GetInputTypePkgName() string {
	dotPos := strings.Index(this.InputType, ".")
	if dotPos > 0 {
		return this.InputType[0:dotPos]
	} else {
		return this.PkgName
	}
}
func (this *TrpcItem) GetInputTypeMsgName() string {
	dotPos := strings.Index(this.InputType, ".")
	if dotPos > 0 {
		return this.InputType[dotPos+1:]
	} else {
		return this.InputType
	}
}
func (this *TrpcItem) GetInputType() string {
	if strings.Contains(this.InputType, ".") {
		return ygen.CamelCase(this.InputTypeFilename) + "Y." + this.InputType
	} else {
		return ygen.CamelCase(this.InputTypeFilename) + "Y." + this.PkgName + "." + this.InputType
	}
}

func (this *TrpcItem) GetInputTypeInterface() string {
	if strings.Contains(this.InputType, ".") {
		return ygen.CamelCase(this.InputTypeFilename) + "Y." + strings.ReplaceAll(this.InputType, ".", ".I")
	} else {
		return ygen.CamelCase(this.InputTypeFilename) + "Y." + this.PkgName + ".I" + this.InputType
	}
}
func (this *TrpcItem) GetOutputTypeImport() string {
	return "import * as " + ygen.CamelCase(this.OutputTypeFilename) + "Y from '@ygen/" + this.OutputTypeFilename + "'"
}
func (this *TrpcItem) GetOutputTypePkgName() string {
	dotPos := strings.Index(this.OutputType, ".")
	if dotPos > 0 {
		return this.OutputType[0:dotPos]
	} else {
		return this.PkgName
	}
}
func (this *TrpcItem) GetOutputTypeMsgName() string {
	dotPos := strings.Index(this.OutputType, ".")
	if dotPos > 0 {
		return this.OutputType[dotPos+1:]
	} else {
		return this.OutputType
	}
}
func (this *TrpcItem) GetOutputType() string {
	if strings.Contains(this.OutputType, ".") {
		return ygen.CamelCase(this.OutputTypeFilename) + "Y." + this.OutputType
	} else {
		return ygen.CamelCase(this.OutputTypeFilename) + "Y." + this.PkgName + "." + this.OutputType
	}
}

func GetRpcVersionFromComment(srvComment string, methodComment string) (verion int) {
	verion = GetVersionFromComment(methodComment)
	if verion > 0 {
		return
	}

	verion = GetVersionFromComment(srvComment)

	return
}
func GetVersionFromComment(comment string) (verion int) {
	commentLines := strings.Split(comment, "\n")
	for _, line := range commentLines {
		if strings.HasPrefix(line, "@ver") {
			verion, _ = strconv.Atoi(line[4:])
			return
		}
	}

	return 0
}

//YgenJsRpc gen js yrpc code
func YgenJsRpc(request *plugin.CodeGeneratorRequest) (genFiles []*plugin.CodeGeneratorResponse_File) {

	for _, fd := range request.GetProtoFile() {

		if len(fd.Service) == 0 {
			continue
		}

		if !ygen.IsProtoFileNeedGenerate(fd.GetName(), request.FileToGenerate) {
			continue
		}

		pkgName := ygen.ProtoPackageName(fd)

		jsYrpcFilename := goutil.ExtractFilename(fd.GetName()) + ".yrpc.ts"
		jsYrpcFileContent := ""
		jsyrpcImport := make(map[string]bool)
		exportDefault := "\nexport default {\n"

		needImportTrpcStream := false

		for _, service := range fd.Service {

			tplContent := make(map[string]interface{})

			unaryCalls := []TrpcItem{}
			unaryNocareCalls := []TrpcItem{}
			clientStreamCalls := []TrpcItem{}
			serverStreamCalls := []TrpcItem{}
			bidiStreamCalls := []TrpcItem{}

			exportDefault += "  " + service.GetName() + ",\n"

			srvComment, _ := ygen.GetProtoServiceLeadingComments(fd, service)
			srvVersion := GetVersionFromComment(srvComment)
			for _, rpc := range service.Method {
				_, methodComment := ygen.GetProtoRpcLeadingComments(fd, service, rpc)
				clientStream := false
				if rpc.ClientStreaming != nil {
					clientStream = *rpc.ClientStreaming
				}
				serverStream := false
				if rpc.ServerStreaming != nil {
					serverStream = *rpc.ServerStreaming
				}
				rpcItem := TrpcItem{
					PkgName:         pkgName,
					ServiceName:     *service.Name,
					MethodName:      *rpc.Name,
					MethodComment:   methodComment,
					InputType:       (*rpc.InputType)[1:],
					OutputType:      (*rpc.OutputType)[1:],
					ClientStreaming: clientStream,
					ServerStreaming: serverStream,
					Version:         GetVersionFromComment(methodComment),
				}
				rpcItem.InputTypeFilename = ygen.GetProtoMsgFileName(request, rpcItem.GetInputTypePkgName(), rpcItem.GetInputTypeMsgName())
				rpcItem.OutputTypeFilename = ygen.GetProtoMsgFileName(request, rpcItem.GetOutputTypePkgName(), rpcItem.GetOutputTypeMsgName())
				jsyrpcImport[rpcItem.GetInputTypeImport()] = true
				jsyrpcImport[rpcItem.GetOutputTypeImport()] = true

				//log.Println("rpcItem:", rpcItem)

				if rpcItem.Version == 0 && srvVersion > 0 {
					rpcItem.Version = srvVersion
				}

				if rpcItem.ClientStreaming {
					if rpcItem.ServerStreaming {
						bidiStreamCalls = append(bidiStreamCalls, rpcItem)
						continue
					} else {
						clientStreamCalls = append(clientStreamCalls, rpcItem)
						continue
					}
				}

				if rpcItem.ServerStreaming {
					serverStreamCalls = append(serverStreamCalls, rpcItem)
					continue
				}

				if rpcItem.OutputType == "Ynocare" {
					unaryNocareCalls = append(unaryNocareCalls, rpcItem)
				} else {
					unaryCalls = append(unaryCalls, rpcItem)
				}
			}

			tplContent["unaryCalls"] = unaryCalls
			tplContent["unaryNocareCalls"] = unaryNocareCalls
			tplContent["clientStreamCalls"] = clientStreamCalls
			tplContent["serverStreamCalls"] = serverStreamCalls
			tplContent["bidiStreamCalls"] = bidiStreamCalls

			tplContent["className"] = *service.Name

			//export jsyrpc func
			for _, trpcItem := range unaryCalls {
				exportDefault += "  " + trpcItem.JsyrpcFunc() + ",\n"
			}
			for _, trpcItem := range unaryNocareCalls {
				exportDefault += "  " + trpcItem.JsyrpcFunc() + ",\n"
			}
			for _, trpcItem := range clientStreamCalls {
				exportDefault += "  " + trpcItem.JsyrpcFunc() + ",\n"
				needImportTrpcStream = true
			}
			for _, trpcItem := range serverStreamCalls {
				exportDefault += "  " + trpcItem.JsyrpcFunc() + ",\n"
				needImportTrpcStream = true
			}
			for _, trpcItem := range bidiStreamCalls {
				exportDefault += "  " + trpcItem.JsyrpcFunc() + ",\n"
				needImportTrpcStream = true
			}

			t := template.Must(template.New("yrpc-js").Parse(jsYrpcClass))

			var tplBytes bytes.Buffer
			if err := t.Execute(&tplBytes, tplContent); err != nil {
				log.Fatal(err)
			}

			oneClassContents := tplBytes.String()
			jsYrpcFileContent += oneClassContents

			//gen one class
		}

		exportDefault += "}\n"

		jsyrpcimportContent := ""
		for imp := range jsyrpcImport {
			jsyrpcimportContent += imp + "\n"
		}

		jsYrpcHeadRpcStreamStr := jsYrpcHeadRpcStream
		if !needImportTrpcStream {
			jsYrpcHeadRpcStreamStr = ""
		}

		genFile := &plugin.CodeGeneratorResponse_File{
			Name:    proto.String(jsYrpcFilename),
			Content: proto.String(jsYrpcHead + jsYrpcHeadRpcStreamStr + jsyrpcimportContent + jsYrpcFileContent + exportDefault),
		}

		genFiles = append(genFiles, genFile)
	}
	return
}
